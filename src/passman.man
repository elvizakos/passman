." Manpage for passgen"
." Contact elvizakos@yahoo.gr for errors or typos."
."    Copyright (C)  2021  Nikos Skarmoutsos."
."    Permission is granted to copy, distribute and/or modify this document"
."    under the terms of the GNU Free Documentation License, Version 1.3"
."    or any later version published by the Free Software Foundation;"
."    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts."
."    A copy of the license is included in the section entitled \"GNU"
."    Free Documentation License\"."

.TH PASSMAN 1 "01 Dec 2021" "%%VERSION%%" "passman Manual"

.SH NAME
passman - Password generator and manager for the command line

.SH SYNOPSIS
.B passman [\fIGLOBAL_ARGUMENTS\fR] COMMAND [\fIARGUMENTS\fR]

.B passman
\-h

.B passman
\-\-help

.B passman
\-v

.B passman
\-\-version

.SH DESCRIPTION
PassMan is a password generator and manager for the linux terminal.

.SH GLOBAL_ARGUMENTS

.PP
.B \-h, \-\-help
.RS 4
Prints help.
.RE

.PP
.B \-v, \-\-version
.RS 4
Prints the version number to the screen.
.RE

.PP
.B \-\-pw=\fIpassword\fR\fB,\fR
.B \-\-password=\fIpassword\fR
.RS 4
Sets the \fIpassword\fR to be used to open the database. If password is needed but this argument isn't used it will prompt for it.

\fBWARNING:\fR This will expose the password as plain text to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password.
.RE

.PP
.B --db=\fIpath\fR\fB,\fR
.B --database=\fIpath\fR
.RS 4
Sets the \fIpath\fR of the database to be used.
.RE

.SH COMMANDS

.PP
.B new
.RS 4
Creates a new entry.

.B \-h, \-\-help
.RS 4
(optional) Show help for new entry command. If this is set, all other arguments will be ignored.
.RE

.B \-\-yes, \-y
.RS 4
(optional) Don't ask for confirmation for creating entry.
.RE

.B \-\-item=\fIname\fR
.RS 4
Sets the \fIname\fR of the entry.
.RE

.B \-\-itmun=\fIusername\fR\fB,\fR
.B \-\-item\-username=\fIusername\fR
.RS 4
(optional) Sets the \fIusername\fR of the entry.
.RE

.B \-\-itmpw=\fIpassword\fR\fB,\fR
.B \-\-item\-password=\fIpassword\fR
.RS 4
(optional) Sets the \fIpassword\fR of the entry.

\fBWARNING:\fR This will expose the password as plain text to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password.
.RE

.B \-\-itmurl=\fIurl\fR\fB,\fR
.B \-\-item\-url=\fIurl\fR
.RS 4
(optional) Sets the \fIurl\fR of the entry.
.RE

.B \-\-itmnt=\fInotes\fR\fB,\fR
.B \-\-item\-notes=\fInotes\fR
.RS 4
(optional) Sets some \fInotes\fR about the entry.
.RE

.RE

.PP
.B get
.RS 4
Shows the data of an entry to the screen.

.B \-h, \-\-help
.RS 4
(optional) Show help for get entry command. If this is set, all other arguments will be ignored.
.RE

.B \-\-item=\fIname\fR
.RS 4
The \fIname\fR of the entry to be displayed.
.RE

.B \-\-clip=\fIfield\fR
.RS 4
Copy a \fIfield\fR from an entry to clipboard. Don't show anything to the screen.

Valid field names are: \fIname\fR, \fIusername\fR, \fIpassword\fR, \fIurl\fR nad \fInotes\fR.
.RE

.B \-\-show\-id
.RS 4
Show the database ID of the entry.
.RE

.B \-\-show\-name
.RS 4
Show entry's name.
.RE

.B \-\-show\-cdate
.RS 4
Show entry's creation date and time.
.RE

.B \-\-show\-mdate
.RS 4
Show entry's last modification date and time.
.RE

.B \-\-show\-username
.RS 4
Show entry's username.
.RE

.B \-\-show\-password
.RS 4
Show entry's password.

\fBWarning: Password will be displayed on screen in plain text\fR
.RE

.B \-\-show\-url
.RS 4
Show entry's URL.
.RE

.B \-\-show\-notes
.RS 4
Show entry's notes.
.RE

.B \-\-columns=\fIcomma_separated_columns\fR
.RS 4
The name of the columns to be displayed, comma separated. Valid column names are: \fIid\fR, \fIname\fR, \fIcdate\fR, \fImdate\fR, \fIusername\fR, \fIpassword\fR, \fIurl\fR and \fInotes\fR.
.RE

.RE

.PP
.B edit
.RS 4
Edits the data of an entry.

.B \-h, \-\-help
.RS 4
(optional) Show help for edit entry command. If this is set, all other arguments will be ignored.
.RE

.B \-\-yes, \-y
.RS 4
(optional) Don't ask for confirmation for modifying the entry.
.RE

.B \-\-item=\fIname\fR
.RS 4
The \fIname\fR of the entry to be modified.
.RE

.B \-\-item\-name=\fInew_name\fR\fB,\fR
.B \-\-itmnn=\fInew_name\fR
.RS 4
(optional) Replace entry's name with \fInew_name\fR.
.RE

.B \-\-item\-username=\fInew_username\fR\fB,\fR
.B \-\-itmun=\fInew_username\fR\fB,\fR
.RS 4
(optional) Replace entry's username with \fInew_username\fR.
.RE

.B \-\-item\-password=\fInew_password\fR\fB,\fR
.B \-\-itmpw=\fInew_password\fR\fB,\fR
.RS 4
(optional) Replace entry's password with \fInew_password\fR.

\fBWARNING:\fR This will expose the password as plain text to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password.
.RE

.B \-\-item\-url=\fInew_url\fR\fB,\fR
.B \-\-itmurl=\fInew_url\fR\fB,\fR
.RS 4
(optional) Replace entry's url with \fInew_url\fR.
.RE

.B \-\-item\-notes=\fInew_notes\fR\fB,\fR
.B \-\-itmnt=\fInew_notes\fR\fB,\fR
.RS 4
(optional) Replace entry's notes with \fInew_notes\fR.
.RE

.B \-\-delete-username, --dun
.RS 4
(optional) Delete the \fIusername\fR field of the selected entry.
.RE

.B \-\-delete-password, --dpw
.RS 4
(optional) Delete the \fIpassword\fR field of the selected entry.
.RE

.B \-\-delete-url, --dul
.RS 4
(optional) Delete the \fIurl\fR field of the selected entry.
.RE

.B \-\-delete-notes, --dnt
.RS 4
(optional) Delete the \fInotes\fR field of the selected entry.
.RE

.RE

.PP
.B del
.RS 4
Deletes an entry.

.B \-h, \-\-help
.RS 4
(optional) Show help for the del entry command. If this is set, all other arguments will be ignored.
.RE

.B \-\-yes, \-y
.RS 4
(optional) Don't ask for confirmation for deleting the entry.
.RE

.B \-\-item=\fIname\fR
.RS 4
The \fIname\fR of the entry to be deleted.
.RE

.RE

.PP
.B list
.RS 4
Shows all or a range of the entries in the database.

.B \-h, \-\-help
.RS 4
(optional) Show help for the "list" subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-b=\fInumber_of_item\fR\fB,\fR
.B \-\-begin=\fInumber_of_item\fR
.RS 4
(optional) Begin from item \fInumber_of_item\fR.
.RE

.B \-l=\fInumber_of_items\fR\fB,\fR
.B \-\-length=\fInumber_of_items\fR
.RS 4
(optional) Limit results to  \fInumber_of_items\fR.
.RE

.RE

.PP
.B gen
.RS 4
Runs \fBpassgen\fR script. All following arguments are passed to this script.

The exit status of \fBpassgen\fR script will be returned.
.RE

.PP
.B clear
.RS 4
Clears data from clipboard and terminal history.

.B \-h, \-\-help
.RS 4
(optional) Show help for this subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-\-clipboard=\fItemp_file\fR
.RS 4
(optional) Reads the \fItemp_file\fR and if it's contents matches with the contents of the clipboard then it clears the clipboard. This will delete the \fItemp_file\fR in any case.
.RE

.B \-\-history
.RS 4
(optional) Clears the terminal history from entries containing commands of \fBpassman\fR.
.RE

.RE

.PP
.B decrypt
.RS 4
Decrypts the selected database.

.B \-h, \-\-help
.RS 4
(optional) Show help for this subcommand. If this is set, all other arguments will be ignored.
.RE

.RE

.PP
.B encrypt
.RS 4
Encrypts the selected database.

.B \-h, \-\-help
.RS 4
(optional) Show help for this subcommand. If this is set, all other arguments will be ignored.
.RE

.RE

.PP
.B export
.RS 4
Export data from database in the selected format.

\fBWARNING\fR: All data, including the passwords of the entries, will be shown in plain text.

If the last argument is a \fIpath\fR to a file the program will export the data into that file.
If the file doesn't exists then it will create the file. If it does exist then it will replace it.
If the path does not exist then it will stop and return error code \fI121\fR.

.B \-h, \-\-help
.RS 4
(optional) Show help for the "export" subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-f=\fIformat\fR\fB,\fR
.B \-\-format=\fIformat\fR
.RS 4
Selecte the export \fIformat\fR. This can be one of the following:

.IP \fIxml\fR
Output data in \fIxml\fR format.
.IP \fIjson\fR
Output data in \fIjson\fR format.
.IP \fIsql\fR
Output data in \fIsql\fR format.
.IP \fIcsv\fR
Output data in \fIcsv\fR format.
.IP \fItxt\fR
Output data in plain text.
.RE

.B \-b=\fInumber_of_item\fR\fB,\fR
.B \-\-begin=\fInumber_of_item\fR
.RS 4
(optional) Begin from item \fInumber_of_item\fR.
.RE

.B \-l=\fInumber_of_items\fR\fB,\fR
.B \-\-length=\fInumber_of_items\fR
.RS 4
(optional) Limit exported items to  \fInumber_of_items\fR.
.RE

.B \-\-overwrite
.RS 4
If output file exists, overwrite it without asking user
.RE

.RE

.PP
.B import
.RS 4

Import data from a file or stdin string to database.

.B \-h, \-\-help
.RS 4
(optional) Shot help for the "import" subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-f=\fIfile\fR\fB,\fR
.B \-\-file=\fIfile\fR
.RS 4
(optional) The path of the \fIfile\fR to read for importing entries.
.RE

.B \-t=\fItype\fR\fB,\fR
.B \-\-type=\fItype\fR
.RS 4
(optional) Set the format \fItype\fR of the import data. If this argument is not set, the program will try to autodetect the correct format type.

Valid \fItypes\fR can be:

.IP \fIxml\fR
Import data from an \fIxml\fR file or string.
.IP \fIjson\fR
Import data from a \fIjson\fR file or string.
.IP \fIsql\fR
Import data from an \fIsql\fR file or string.
.IP \fIcsv\fR
Import data from a \fIcsv\fR file or string.
.RE

.B \-\-, \-\-stdin
.RS 4
(optional) Read data from the \fIstdin\fR string.
.RE

.RE

.PP
.B build
.RS 4
Creates a database file to be used for storing passwords and adds basic settings to it.

.B \-h, \-\-help
.RS 4
(optional) Show help for this subcommand. If this is set, all other arguments will be ignored.
.RE

.RE

.PP
.B backup
.RS 4
Show list of backups for an entry, restore a backup or view backup's data.

.B \-h, \-\-help
.RS 4
(optional) Show help for this subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-\-entry=\fIentry\fR
.RS 4
Select the \fIentry\fR to work with it's backups.
.RE

.B \-\-list
.RS 4
Print a list of the backups for the selected entry.
.RE

.B \-\-restore=\fInumber\fR
.RS 4
Restore the the data of selected item \fInumber\fR to the selected entry.
.RE

.B \-\-view=\fInumber\fR
.RS 4
View the data of the selected item \fInumber\fR of the selected entry.
.RE

.B \-rp, \-\-reveal-password
.RS 4
Reveal passwords on printing data to the screen.
.RE

.B \-y, \-\-yes
.RS 4
Don't ask for confirmation when restoring a backup.
.RE

.RE

.PP
.B setting
.RS 4
Get and change the values of settings keys in database.

.B \-h, \-\-help
.RS 4
(optional) Show help for the "setting" subcommand. If this is set, all other arguments will be ignored.
.RE

.B \-\-name=\fIsetting_name\fR
.RS 4
Give the \fIsetting_name\fR of the setting to change it's value ( to be used with the \fI\-\-set\fR argument).
.RE

.B \-\-delete=\fIsetting_name\fR
.RS 4
Delete the value of a setting defined by the \fIsetting_name\fR.
.RE

.B \-\-set=\fIvalue\fR
.RS 4
Set the \fIvalue\fR for the setting selected using the \fI\-\-name\fR argument.
.RE

.B \-\-get=\fIsetting_name\fR
.RS 4
Get the value of the \fIsetting_name\fR.
.RE

.B \-\-list
.RS 4
Get a list of the setting names in database.
.RE

.RE

.SH settings.sh
This file is for storing the settings of \fBpassman\fR and \fBpassgen\fR scripts.
It can be in the locations \fI~/.local/etc/passman/settings.sh\fR
for the current user and  \fI/etc/passman/settings.sh\fR for all users.

The following variables can be used in this file:

.PP
.B dbfilepath
.RS 4
This variable sets the path of the database file.
Default value is \fI"~/.config/passman/passman.db"\fR.
.RE

.PP
.B numberOfEncryptionTimes
.RS 4
This variable sets the number of times to encrypt and decrypt the database file.
Default value is 2.
.RE

.PP
.B passgencmd
.RS 4
This variable sets the location of \fBpassgen\fR script.
.RE

.PP
.B encryptCMD
.RS 4
This variable sets the command to be used for the encryption of database.

In the variable can be used:

.IP %%DBPATH%%
Replaced with the path of the database.
.IP %%DBPASS%%
Replaced with the password.

.RE

.PP
.B decryptCMD
.RS 4
This variable sets the command to be used for the decryption of database.

In the variable can be used:

.IP %%DBPATH%%
Replaced with the path of the database.
.IP %%DBPASS%%
Replaced with the password.

.RE

.SH EXIT STATUS

.PP
.B 120
.RS 4
Imported file type is not supported.
.RE

.PP
.B 121
.RS 4
Database file does not exist or is inaccessible.
.RE

.PP
.B 122
.RS 4
For new command, entry already exists. For edit and delete commands, entry does not exist.
.RE

.PP
.B 123
.RS 4
Dependency does not exist.
.RE

.PP
.B 124
.RS 4
Wrong password for database.
.RE

.PP
.B 125
.RS 4
Unknown command or argument.
.RE

.SH SEE ALSO
passgen

.SH BUGS
No known bugs.

.SH AUTHOR
Nikos Skarmoutsos (elvizakos@yahoo.gr)
