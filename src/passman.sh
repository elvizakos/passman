#!/bin/bash

## Project version
scriptVersion="%%VERSION%%"

## Number of times to encrypt a file 
numberOfEncryptionTimes=2

## Database default path
dbfilepath=~/.local/share/passman/passman.db

## Database temporary file
tmpdbfilename=""

## Temporary file
tmpdbsfilename="/tmp/passman.s"

## Temporary file
tmpdbpfilename="/tmp/passman.p"

## Show message and wait users confirmation about entry's data
confirmentry="true"

## Location of passgen command
passgencmd="$(dirname $0)/passgen"

## If true ask for a password if not given in arguments
askpasswordifnotinargs="false"

## The command to be used for encrypting the database.
encryptCMD="cat \"%%DBPATH%%\" | gpg --passphrase \"%%DBPASS%%\" --batch --quiet --yes -c -o \"%%DBPATH%%\""
#encryptCMD="openssl des3 -in \"%%DBPATH%%\" -out \"%%DBPATH%%\" -pass pass:\"%%DBPASS%%\""

## The command to be used for decrypting the database.
decryptCMD="cat \"%%DBPATH%%\" | gpg --passphrase \"%%DBPASS%%\" --batch --quiet --yes -d -o \"%%DBPATH%%\""
#decryptCMD="openssl des3 -in \"%%DBPATH%%\" -out \"%%DBPATH%%\" -d -pass pass:\"%%DBPASS%%\""

## Sets if getting data from and entry should be printed on the screen or copied to clipboard.
copytoclipboard="false"

## Set the number of minutes after of which the clipboard is going to be cleared. Set "false" for never clearing the clipboard.
clearafter="5"

## Use asterisk characters "*" to hide passwords.
hidepass="true"

## Used to check if db has changed and needs to be saved.
dbchanged="false"

# Formats the given error string and outputs it to stderr
# @param {string} $1 - The error string that describes the error
function printError () {
	echo -e "\e[31mERROR:\e[m $1" > /dev/stderr
}

####
## DATABASE FUNCTIONS

# Creates a database file
# @param {path} $1 - The path to the database file
function createDatabase () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	[ ! -w "$(dirname "$dbpath")" ] && return 121
	echo "$dbpath"
	sqlite3 "$dbpath" "CREATE TABLE passwords ( pass_id INTEGER PRIMARY KEY, pass_name TEXT, pass_creation_date TEXT, pass_modification_date TEXT, pass_username TEXT, pass_password TEXT, pass_url TEXT, pass_notes TEXT ); CREATE TABLE settings ( setting_id INTEGER PRIMARY KEY, setting_name TEXT, setting_value TEXT ); CREATE TABLE backups ( bpass_id INTEGER PRIMARY KEY, bpass_pass_id INTEGER, bpass_backup_date TEXT, bpass_name TEXT, bpass_creation_date TEXT, bpass_modification_date TEXT, bpass_username TEXT, bpass_password TEXT, bpass_url TEXT, bpass_notes TEXT );" 2> /dev/null
	return "$?"

	# table passwords
	#		pass_id
	#		pass_name
	#		pass_creation_date
	#		pass_modification_date
	#		pass_username
	#		pass_password
	#		pass_url
	#		pass_notes

	# table settings
	#		setting_id
	#		setting_name
	#		setting_value

	# table backups
	#		bpass_id
	#		bpass_pass_id
	#		bpass_backup_date
	#		bpass_name
	#		bpass_creation_date
	#		bpass_modification_date
	#		bpass_username
	#		bpass_password
	#		bpass_url
	#		bpass_notes
}

# Encrypts the given database file using a password
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to encrypt the file
# @errorCode 0 - The file has been encrypted
# @errorCode 121 - The file does not exist
# @errorCode 123 - The command gpg does not exist
function encryptDB () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"
	[ ! -f "$dbpath" ] && return 121
	[ -z "$(command -v gpg)" ] && return 123

	ecmd="${encryptCMD//%%DBPATH%%/$dbpath}"
	ecmd="${ecmd//%%DBPASS%%/$dbpass}"

	# for i in {1..$numberOfEncryptionTimes}; do
	for i in $(seq "$numberOfEncryptionTimes"); do		
		cat "$dbpath" | gpg --passphrase "$dbpass" --batch --quiet --yes -c -o "$dbpath"
	done
}

# Decrypts the given database file using a password
# @param {path} $1 - The path to database file
# @param {string} $2 - The password to encrypt the file
# @errorCode 0   - The file has been decrypted
# @errorCode 121 - The file does not exist
# @errorCode 123 - The command gpg does not exist
# @errorCode 124 - The database password is not correnct
function decryptDB () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"
	[ ! -f "$dbpath" ] && return 121
	[ -z "$(command -v gpg)" ] && return 123

	dcmd="${decryptCMD//%%DBPATH%%/$dbpath}"
	dcmd="${dcmd//%%DBPASS%%/$dbpass}"

	#for i in {1..$numberOfEncryptionTimes}; do
	for i in $(seq "$numberOfEncryptionTimes"); do
		cat "$dbpath" | gpg --passphrase "$dbpass" --batch --quiet --yes -d -o "$dbpath"
		if [ "$?" != "0" ]; then
			printError "Database password is not correct."
			return 124
		fi
		# openssl des3 -in "$dbpath" -out "$tmpfilename" -d -pass pass:"$dbpass"
	done
}

# Decrypts the given database file using a password to a temporary location. Echoes the location to temp file
# @param {path} $1 - The path to database file
# @param {string} $2 - The password to encrypt the file
# @errorCode 0   - The file has been decrypted
# @errorCode 121 - The file does not exists
# @errorCode 123 - The command gpg does not exists
# @errorCode 124 - The database password is not correnct
function tmpDecryptDB () {
	local dbpath=""
	local dbpass=""

	if [ -f "$tmpdbsfilename" ]; then
		echo "/tmp/passman.$(cat "$tmpdbsfilename")"
		return 0
	fi

	if [ -z ${tmpdbfilename+x} ] && [ -f "$tmpdbfilename" ]; then
		# If db is already decrypted
		echo "$tmpdbfilename"
		return 0
	fi

	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"

	tmpdbfilename="$(mktemp "/tmp/passman.XXXXXX")"

	[ ! -f "$dbpath" ] && return 121
	[ -z "$(command -v gpg)" ] && return 123
	cp "$dbpath" "$tmpdbfilename"

	dcmd="${decryptCMD//%%DBPATH%%/$tmpdbfilename}"
	dcmd="${dcmd//%%DBPASS%%/$dbpass}"

	# for i in {1.."$numberOfEncryptionTimes"}; do
	for i in $(seq "$numberOfEncryptionTimes"); do
		cat "$tmpdbfilename" | gpg --passphrase "$dbpass" --batch --quiet --yes -d -o "$tmpdbfilename" 2> /dev/null
		if [ "$?" != "0" ]; then
			rm "$tmpdbfilename"
			printError "Database password is not correct."
			return 124
		fi
	done

	minutesDec="$(getSetting "$tmpdbfilename" "$dbpassowrd" "keep_db_unencrypted_for_minutes")"

	if [ ! "$minutesDec" == "" ] && [ "$minutesDec" -gt "0" ]; then
		echo "${tmpdbfilename:13}" > "$tmpdbsfilename"
		echo -n "$dbpass" > "$tmpdbpfilename"
		echo "$0 clear --tmpdb=\"$tmpdbfilename\" " | at now + "$minutesDec" minute 2> /dev/null
	fi

	echo "$tmpdbfilename"
}

# Adds an item to database
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @param {string} $3 - The items name
# @param {string} $4 - The items username
# @param {string} $5 - The items password
# @param {string} $6 - The items URL
# @param {string} $7 - Notes
# @errorCode 0   - Successfuly saved the item
# @errorCode 121 - The database file does not exists or is not accessible
# @errorCode 122 - There is already an entry with the same name
# @errorCode 124 - The password for database file is wrong
function addItem () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"
	itemName="$(echo "$3" | base64)"
	itemUsername="$(echo "$4" | base64)"
	itemPassword="$(echo "$5" | base64)"
	itemUrl="$(echo "$6" | base64)"
	itemNotes="$(echo "$7" | base64)"
	itemDate="$(date +'%Y-%m-%d %H:%M:%S')"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	[ "${dbpass}" != "" ] && decryptDB "$dbpath" "$dbpass"

	# Test if there is already and entry with the same name
	ret="$(sqlite3 "$dbpath" "SELECT pass_name FROM passwords WHERE pass_name = '${itemName}'")"
	[ "$ret" != "" ] && return 122

	sqlite3 "$dbpath" "INSERT INTO passwords (pass_name,pass_creation_date,pass_modification_date,pass_username,pass_password,pass_url,pass_notes)VALUES(\"${itemName}\",\"${itemDate}\",\"${itemDate}\",\"${itemUsername}\",\"${itemPassword}\",\"${itemUrl}\",\"${itemNodes}\");" 2> /dev/null

	if [ "$?" == "0" ]; then
 		[ "${dbpass}" != "" ] && encryptDB "$dbpath" "$dbpass" || echo -n ""
		return $?
	else
		printError "Couldn't save entry... Abording"
		return 124
	fi
}

# Gets an item from database
# @param {path} $1     - The path to the database file
# @param {string} $2   - The password to decrypt the file
# @param {string} $3   - The name of the item to get
# @param {string} [$4] - The columns to return, comma separated
# @param {string} [$5] - If is "true" then hide password using astrerink character "*"
# @errorCode 121 - The database file does not exists or is not accessible
# @errorCode 3 - There's not an entry with that name
function getItem () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"
	itemName="$(echo "$3" | base64)"

	hidepasss="$5"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	tmpdbfilename="$(tmpDecryptDB "$dbpath" "$dbpass")"

	[ "$?" != "0" ] && return $?

	ret="$(sqlite3 "$tmpdbfilename" "SELECT pass_id, pass_name, pass_creation_date, pass_modification_date, pass_username, pass_password, pass_url, pass_notes FROM passwords WHERE pass_name = '${itemName}';")"

	# If there is no entry returned, return error code 3
	[ "$ret" == "" ] && return 122

	oIFS="$IFS"
	IFS='|' read -r -a ret <<< "$ret"
	itemID="${ret[0]}"
	itemName="$(echo "${ret[1]}" | base64 -d)"
	itemCDate="${ret[2]}"
	itemMDate="${ret[3]}"
	itemUsername="$(echo "${ret[4]}" | base64 -d)"
	itemPassword="$(echo "${ret[5]}" | base64 -d)"
	itemUrl="$(echo "${ret[6]}" | base64 -d)"
	itemNotes="$(echo "${ret[7]}" | base64 -d)"
	IFS="$oIFS"

	[ "$hidepasss" == "true" ] && itemPassword="$(echo "$itemPassword" | sed -r 's/./*/g')"

	if [ "$4" == "" ]; then
		echo "name: $itemName"
		echo "username: $itemUsername"
		echo "password: $itemPassword"
		echo "url: $itemUrl"
		echo "Notes: $itemNotes"
	else
		selectColumns="$4"
		declare -A cselected
		for i in ${selectColumns//,/ }; do
			cselected[$i]="true"
		done

		[ "${cselected[id]}" == "true" ] && echo "ID: $itemID"
		[ "${cselected[name]}" == "true" ] && echo "name: $itemName"
		[ "${cselected[cdate]}" == "true" ] && echo "creation date: $itemCDate"
		[ "${cselected[mdate]}" == "true" ] && echo "modification date: $itemMDate"
		[ "${cselected[username]}" == "true" ] && echo "username: $itemUsername"
		[ "${cselected[password]}" == "true" ] && echo "password: $itemPassword"
		[ "${cselected[url]}" == "true" ] && echo "url: $itemUrl"
		[ "${cselected[notes]}" == "true" ] && echo "Notes: $itemNotes"
	fi

}

# Returns "true" if an item with the specified name, exists in the database.
# Otherwise it returns "false".
# @param {path} $1 - The path ot the database file
# @param {string} $2 - The password to decrypt the database file
# @param {string} $3 - The name of the item
# @errorCode 121 - The databse file does not exists or is not accessible
function itemExists () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	itemName="$(echo "$2" | base64)"

	[ ! -f "$dbpath" ] && return 121

	ret="$(sqlite3 "$dbpath" "SELECT pass_name, pass_username, pass_password, pass_url, pass_notes FROM passwords WHERE pass_name = '${itemName}';")"

	[ "$ret" == "" ] && echo "false" || echo "true"
}

# Gets a specific column from an item from database
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @param {string} $3 - The name of the item to get
# @param {string} $4 - The name of the column to get (name, username, password, url, notes)
# @errorCode 121 - The database file does not exists or is not accessible
# @errorCode 122 - There's not an entry with that name
function getItemsEntry () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"
	itemName="$(echo "$3" | base64)"
	column="$4"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	if [ "$dbpass" == "" ]; then
		tmpdbfilename="$dbpath"
	else
		tmpdbfilename="$(tmpDecryptDB "$dbpath" "$dbpass")"
		[ "$?" != "0" ] && return $?
	fi

	ret="$(sqlite3 "$tmpdbfilename" "SELECT pass_name, pass_username, pass_password, pass_url, pass_notes FROM passwords WHERE pass_name = '${itemName}';")"

	# If there is no entry returned, return error code 3
	[ "$ret" == "" ] && return 122

	oIFS="$IFS"
	IFS='|' read -r -a ret <<< "$ret"
	itemName="$(echo "${ret[0]}" | base64 -d)"
	itemUsername="$(echo "${ret[1]}" | base64 -d)"
	itemPassword="$(echo "${ret[2]}" | base64 -d)"
	itemUrl="$(echo "${ret[3]}" | base64 -d)"
	itemNotes="$(echo "${ret[4]}" | base64 -d)"
	IFS="$oIFS"

	if [ "$column" == "name" ]; then
		echo "$itemName"
	elif [ "$column" == "username" ]; then
		echo "$itemUsername"
	elif [ "$column" == "password" ]; then
		echo "$itemPassword"
	elif [ "$column" == "url" ]; then
		echo "$itemUrl"
	elif [ "$column" == "notes" ]; then
		echo "$itemNotes"
	fi

}

# Modify an item in the database
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @param {string} $3 - The items name to be modified
# @param {string} $4 - A comma separated list of the properties to be modified
# @param {string} $5 - The items new name
# @param {string} $6 - The items new username
# @param {string} $7 - The items new password
# @param {string} $8 - The items new URL
# @param {string} $9 - The items new Notes
# @errorCode 0   - Successfuly saved the item
# @errorCode 121 - The database file does not exists or is not accessible
# @errorCode 124 - The password for database file is wrong
function editItem () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	entryname="$3"


	oIFS="$IFS"
	IFS=',' read -r -a deletefields <<< "${10}"
	IFS="$oIFS"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	args=$(echo "$4" | tr "," "\n")
	# shift 4

	sql1="pass_modification_date='$(date +'%Y-%m-%d %H:%M:%S')'"

	for i in ${args[@]}; do

		if [ "$i" == "name" ]; then
			sql1+=", pass_name='$(echo "$5" | base64)'"
		elif [ "$i" == "username" ]; then
			sql1+=", pass_username='$(echo "$6" | base64)'"
		elif [ "$i" == "password" ]; then
			sql1+=", pass_password='$(echo "$7" | base64)'"
		elif [ "$i" == "url" ]; then
			sql1+=", pass_url='$(echo "$8" | base64)'"
		elif [ "$i" == "notes" ]; then
			sql1+=", pass_notes='$(echo "$9" | base64)'"
		fi

	done

	[ "${deletefields[0]}" == "true" ] && sql1+=", pass_username=''"
	[ "${deletefields[1]}" == "true" ] && sql1+=", pass_password=''"
	[ "${deletefields[2]}" == "true" ] && sql1+=", pass_url=''"
	[ "${deletefields[3]}" == "true" ] && sql1+=", pass_notes=''"

	sql1="UPDATE passwords SET $sql1 WHERE pass_name='$(echo "$entryname" | base64)';"

	decryptDB "$dbpath" "$dbpass"

	if [ "$(getSetting "$dbpath" "$dbpassword" "keep_backup_on_edit_entry")" == "true" ]; then
		backupItem "$dbpath" "$dbpass" "$entryname"
	fi

	sqlite3 "$dbpath" "$sql1" 2> /dev/null

	if [ "$?" == "0" ]; then
 		encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't save entry... Abording"
		return 124
	fi
}

# Delete an item in the database
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @param {string} $3 - The item's name to be deleted
# @errorCode 121 - The database file does not exists or is not accessible
# @errorCode 124 - The password for database file is wrong
function deleteItem () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"
	entryname="$3"

	# Test if database file exists and if not return error code 2
	[ ! -f "$dbpath" ] && return 121

	sql="DELETE FROM passwords WHERE pass_name='$(echo "$entryname" | base64)';"

	decryptDB "$dbpath" "$dbpass"

	[ "$(getSetting "$dbpath" "$dbpass" "keep_backup_on_delete_entry")" == "true"] && backupItem "$dbpath" "$dbpass" "$entryname"

	sqlite3 "$dbpath" "$sql" 2> /dev/null

	if [ "$?" == "0" ]; then
		encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't delete the entry... Abording"
		return 124
	fi
}

# Get a list of the names of all password records
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @param {number} $4 - 
# @errorCode 121 - The database file does not exists or is not accessible
function getListOfItems () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"

	beginitem="$3"
	listlength="$4"

	limitstr=""

	if [ "$beginitem" != "" ] && [[ "$beginitem" =~ ^[0-9]$ ]]; then
		limitstr="LIMIT $beginitem"
		[ "$listlength" != "" ] && [[ "$listlength" =~ ^[0-9]$ ]] && limitstr+=",$listlength"
	fi

	# Test if database file exists and if not return error code 2
	[ ! -f "$dbpath" ] && return 121

	tmpdbfilename="$(tmpDecryptDB "$dbpath" "$dbpass")"
	[ "$?" != "0" ] && return $?

	ret="$(sqlite3 "$tmpdbfilename" "SELECT pass_name FROM passwords ${limitstr} ;")"
	# rm "$tmpdbfile"
	i=0
	while read -r line; do
		i=$(("$i" + 1))
		echo -n "$i - "
		echo -e "$line" | base64 -d
	done < <(echo "${ret}")
}

# Get a list of the names of all password records and export as B64
# @param {path} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the file
# @errorCode 121 - The database file does not exists or is not accessible
function getListOfItemsB64 () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi
	dbpass="$2"

	beginitem="$3"
	listlength="$4"

	limitstr=""

	if [ "$beginitem" != "" ] && [[ "$beginitem" =~ ^[0-9]$ ]]; then
		limitstr="LIMIT $beginitem"
		[ "$listlength" != "" ] && [[ "$listlength" =~ ^[0-9]$ ]] && limitstr+=",$listlength"
	fi

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	tmpdbfilename="$(tmpDecryptDB "$dbpath" "$dbpass")"
	[ "$?" != "0" ] && return $?

	# pass_id
	# pass_name
	# pass_creation_date
	# pass_modification_date
	# pass_username
	# pass_password
	# pass_url
	# pass_notes

	ret="$(sqlite3 "$tmpdbfilename" "SELECT * FROM passwords ${limitstr} ;")"
	rm "$tmpdbfilename"
	i=0
	while read -r line; do
		i=$(("$i" + 1))
		echo -n "$i"
		echo -e "$line"
	done < <(echo "${ret}")
}

# Keep backup of an item
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file
# @param {string} $3 - The name of the entry
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Error on copying entry to backup.
function backupItem () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	entryname="$3"
	entrynameb64="$(echo "$entryname" | base64)"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	backupItemLimit="$(getSetting "$dbpath" "$dbpassword" "number_of_backup_items_per_entry")"
	if [ "$backupItemLimit" -gt "0" ]; then
		sqlbud="DELETE FROM backups WHERE bpass_name = '${entrynameb64}' AND bpass_id NOT IN (SELECT bpass_id FROM backups WHERE bpass_name = '${entrynameb64}' ORDER BY bpass_id LIMIT ${backupItemLimit});"
		sqlite3 "$dbpath" "$sqlbud" 2> /dev/null
	fi

	sqlbu="INSERT INTO backups ( bpass_id, bpass_pass_id, bpass_backup_date, bpass_name, bpass_creation_date, bpass_modification_date, bpass_username, bpass_password, bpass_url, bpass_notes ) SELECT NULL, pass_id, '$(date +'%Y-%m-%d %H:%M:%S')', pass_name, pass_creation_date, pass_modification_date, pass_username, pass_password, pass_url, pass_notes FROM passwords WHERE pass_name = '${entrynameb64}';"
	sqlite3 "$dbpath" "$sqlbu" 2> /dev/null
	sqlstatus="$?"

	if [ "$sqlstatus" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
		return 0
	else
		printError "Couldn't backup entry... Abording"
		return 124
	fi

}

# Return the list of backups for the given entry
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file
# @param {string} $3 - The name of the entry.
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Error on getting backup list.
function getListOfBackupsForEntry () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	entryname="$3"
	entrynameb64="$(echo "$entryname" | base64)"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	# sql="SELECT bpass_id, bpass_pass_id, bpass_backup_date FROM backups WHERE bpass_name = '${entrynameb64}';"
	sql="SELECT bpass_id, bpass_backup_date FROM backups WHERE bpass_name = '${entrynameb64}';"

	ret="$(sqlite3 "$dbpath" "$sql" 2> /dev/null)"
	while read line; do

		sIFS="$IFS"
		IFS=$'|'
		bline=( $line )
		echo "	${bline[0]}) ${bline[1]}"
		
	done < <(echo "$ret")

	if [ "$?" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't get backup list... Abording"
		return 124
	fi
}

# Show the data of a backup entry
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file, if it's encrypted. Otherwise this argument will be ignored
# @param {string} $3 - The name of the entry
# @param {number} $4 - The number of the backup item
# @param {true|false} $5 - The string "false" for revealing the password or false for replace every character with the "*" character
# @errorCode 121 - The database file does not exist or is not accessible.
# @errorCode 124 - Error reading the databse
function viewBackupEntryData () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	entryname="$3"
	entrynameb64="$(echo "$entryname" | base64)"

	backupitem="$4"

	hidepassword="$5"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 chipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	sql="SELECT * FROM backups WHERE bpass_id = '${backupitem}';"

	ret="$(sqlite3 "$dbpath" "$sql" 2> /dev/null)"
	while read line; do

		sIFS="$IFS"
		IFS=$'|'
		bline=( $line )
		#echo -e "	${bline[0]}" # backup item id
		#echo -e "	${bline[1]}" # entry id
		echo -e "	backup date:  \e[33G${bline[2]}" # backup date
		echo -e "	entry's name: \e[33G$(echo "${bline[3]}" | base64 -d)" # name
		echo -e "	creation date: \e[33G${bline[4]}" # creation date
		echo -e "	last modification date: ${bline[5]}" # mod date
		echo -e "	username: \e[33G$(echo "${bline[6]}" | base64 -d)" # username
		if [ "$hidepassword" == "false" ]; then
			echo -e "	password: \e[33G$(echo "${bline[7]}" | base64 -d)" # password
		else
			echo -e "	password: \e[33G$(echo "${bline[7]}" | base64 -d | sed 's/./*/g')" # password
		fi
		echo -e "	url: \e[33G$(echo "${bline[8]}" | base64 -d)" # url
		echo -e "	notes: \e[33G$(echo "${bline[9]}" | base64 -d)" # notes
		
	done < <(echo "$ret")

	if [ "$?" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't get backup list... Abording"
		return 124
	fi
}

# Restore en entry back to a point in backup table.
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file, if it's encrypted. Otherwise this argument will be ignored
# @param {string} $3 - The name of the entry that will be restored
# @param {number} $4 - The number of the backup item to restore
# @errorCode 121 - The databse file does not exist or is not accessible.
# @errorCode 124 - Error restoring the entry.
function restoreBackup () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	entryname="$3"
	entrynameb64="$(echo "$entryname" | base64)"

	backupitem="$4"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] &&  return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 chipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	[ "$(getSetting "$dbpath" "$dbpass" "keep_backup_on_delete_entry")" == "true" ] && backupItem "$dbpath" "$dbpass" "$entryname"

	sqlrsr="INSERT OR REPLACE INTO passwords ( pass_id, pass_name, pass_creation_date, pass_modification_date, pass_username, pass_password, pass_url, pass_notes ) SELECT bpass_pass_id, bpass_name, bpass_creation_date, '$(date +'%Y-%m-%d %H:%M:%S')', bpass_username, bpass_password, bpass_url, bpass_notes FROM backups WHERE bpass_id = '${backupitem}' LIMIT 1;"
	sqlite3 "$dbpath" "$sqlrsr" #2> /dev/stdout
	sqlstatus="$?"

	if [ "$sqlstatus" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
		return 0
	else
		printError "Couldn't restore entry... Abording $sqlstatus"
		return 124
	fi

}

# Add or modify a setting in the database
# @param {string} $1 - The path to the database file
# @param {string} $2 - The pasword to decrypt the database file
# @param {string} $3 - The setting name
# @param {string} $4 - The setting value.
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Can't save to database.
function setSetting () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	settingName="$3"
	settingValue="$4"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	sql="INSERT OR REPLACE INTO settings (setting_id, setting_name, setting_value) VALUES ( (SELECT setting_id FROM settings WHERE setting_name = '${settingName}'), '${settingName}', '${settingValue}' );"

	sqlite3 "$dbpath" "$sql" 2> /dev/null

	if [ "$?" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
		return 0
	else
		printError "Couldn't save setting... Abording"
		return 124
	fi

}

# Get the value of a setting entry in database
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file
# @param {string} $3 - The setting name
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Can't get data from database.
function getSetting () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	settingName="$3"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	sql="SELECT setting_value FROM settings WHERE setting_name = '${settingName}';"

	ret=$(sqlite3 "$dbpath" "$sql")
	qstat="$?"

	echo "$ret"

	if [ "$qstat" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't get setting..."
		return 124
	fi

}

# Get a list of the setting names entries in database
# @param {string} $1 - The path to the database file
# @param {string} $2 - The password to decrypt the database file
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Can't get data from database.
function listSettings () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	sql="SELECT setting_name FROM settings;"

	ret=$(sqlite3 "$dbpath" "$sql")
	qstat="$?"

	echo "$ret"

	if [ "$qstat" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't get settings list..."
		return 124
	fi

}

# Delete a setting from the databse
# @param {string} $1 - The path to the databse file
# @param {string} $2 - The password to decrypt the database file
# @param {string} $3 - The setting name
# @errorCode 121 - The database file does not exist or is not accessible
# @errorCode 124 - Couldn't delete entry.
function delSetting () {
	if [ "$1" == "-" ]; then
		dbpath="$dbfilepath"
	else
		dbpath="$1"
	fi

	dbpass="$2"

	settingName="$3"

	# Test if database file exists and if not return error code 121
	[ ! -f "$dbpath" ] && return 121

	dbisencrypted="false"
	filetype="$(file -b "$dbpath")"

	if [ "$filetype" == "GPG symmetrically encrypted data (AES256 cipher)" ]; then
		dbisencrypted="true"
	elif [ "${filetype:0:19}" == "SQLite 3.x database" ]; then
		dbisencrypted="false"
	else
		printError "Doesn't seem to be a database file."
		return 124
	fi

	[ "$dbisencrypted" == "true" ] && decryptDB "$dbpath" "$dbpass"

	sql="DELETE FROM settings WHERE setting_name = '${settingName}';"

	sqlite3 "$dbpath" "$sql" 2> /dev/null
	qstat="$?"

	if [ "$qstat" == "0" ]; then
		[ "$dbisencrypted" == "true" ] && encryptDB "$dbpath" "$dbpass"
	else
		printError "Couldn't save setting... Abording"
		return 124
	fi

}

## /DATABASE FUNCTIONS
####

####
## CLIPBOARD FUNCTIONS

# Push a string to clipboard
# @param {string} $1 - The string to be pushed to the clipboard
# @errorCode 123 - If there is no method to copy to clipboard installed on the system
function clipcopy () {
	if [ -e "/dev/clipboard" ]; then             ## cygwin style clipboard
		echo -n "$1" > /dev/clipboard
	elif [ ! -z "$(command -v "xclip")" ]; then  ## xclip
		echo -n "$1" | xclip -selection clip-board -i
	elif [ ! -z "$(command -v "xsel")" ]; then   ## xsel
		echo -n "$1" | xsel -ib
	elif [ ! -z "$(command -v "pbcopy")" ]; then ## Mac OS X
		echo -n "$1" | pbcopy
	else
		return 123
	fi
}

# Print the string from clipboard
# @errorCode 123 - If there is no method to paste from clipboard installed on the system
function clippaste () {
	if [ -e "/dev/clipboard" ]; then			  ## cygwin style clipboard
		cat /dev/clipboard
	elif [ ! -z "$(command -v "xclip")" ]; then	  ## xclip
		xclip -selection clip-board -o
	elif [ ! -z "$(command -v "xsel")" ]; then	  ## xsel
		xsel -b
	elif [ ! -z "$(command -v "pbpaste")" ]; then ## Mac OS X
		pbpaste
	else
		return 123
	fi
}

## /CLIPBOARD FUNCTIONS
####

function clearTraces () {

	export DISPLAY=:0.0
	ppass="$(cpaste 2>/dev/null)"
	mpass="$(cat "$2")"
	rm "$2"
	[ "$ppass" == "$mpass" ] && clipcopy " "

	tmpfilename="$(mktemp)"
	echo -e "$ret" > $tmpfilename
	echo "$0 --clear-memory \"$tmpfilename\"" | at now + "$clearafter" minute 2> /dev/null
}

# Clears data before exit
function clearDataOnExit () {

	if [ ! -f "$tmpdbsfilename" ] && [ -f "$tmpdbfilename" ]; then
		rm "$tmpdbfilename"
	fi

}

# Print help about GLOBAL ARGUMENTS
function print_global_arguments_help () {
	echo -e "\e[1;4mGLOBAL ARGUMENTS:\e[m"
	echo -e "  \e[1m--password[=]<password>\e[m"
	echo -e "\e[20GGive the \e[4m<password>\e[m to open the database."
	echo -e "\e[20G\e[1mWARNING:\e[m This will expose the password to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password."
	echo -e "  \e[1m--db[=]<path>|--database[=]<path>\e[m"
	echo -e "\e[20GSet the \e[4m<path>\e[m of the database file to open."
}

# Get entry's data and return an xml representation.
# @param {number} $1 - Entry's id
# @param {string} $2 - Entry's name
# @param {date}   $3 - Entry's creation date
# @param {date}   $4 - Entry's modification date
# @param {string} $5 - Entry's username
# @param {string} $6 - Entry's password
# @param {string} $7 - Entry's URL
# @param {string} $8 - Entry's Notes
function entryToXML () {

	name="$(echo "$2" | sed 's/&/&amp;/g' | sed 's/</&lt/g')"
	username="$(echo "$5" | sed 's/&/&amp;/g' | sed 's/</&lt/g')"
	password="$(echo "$6" | sed 's/&/&amp;/g' | sed 's/</&lt/g')"
	url="$(echo "$7" | sed 's/&/&amp;/g' | sed 's/</&lt/g')"
	notes="$(echo "$8" | sed 's/&/&amp;/g' | sed 's/</&lt/g')"

	echo "	<Entry>"
	echo "		<ID>$1</ID>"
	echo "		<Name>$name</Name>"
	echo "		<CreationDate>$3</CreationDate>"
	echo "		<ModificationDate>$4</ModificationDate>"
	echo "		<UserName>$username</UserName>"
	echo "		<Password>$password</Password>"
	echo "		<URL>$url</URL>"
	echo "		<Notes>$notes</Notes>"
	echo "	</Entry>"
}

# Get entry's data and return a JSON representation.
# @param {number} $1 - Entry's id
# @param {string} $2 - Entry's name
# @param {date}   $3 - Entry's creation date
# @param {date}   $4 - Entry's modification date
# @param {string} $5 - Entry's username
# @param {string} $6 - Entry's password
# @param {string} $7 - Entry's URL
# @param {string} $8 - Entry's Notes
function entryToJSON () {
	name="$(echo "$2" | sed 's/"/\\"/g' | sed 's/\\\\/\\\\\\\\/g')"
	username="$(echo "$5" | sed 's/"/\\"/g' | sed 's/\\\\/\\\\\\\\/g')"
	password="$(echo "$6" | sed 's/"/\\"/g' | sed 's/\\\\/\\\\\\\\/g')"
	url="$(echo "$7" | sed 's/"/\\"/g' | sed 's/\\\\/\\\\\\\\/g')"
	notes="$(echo "$8" | sed 's/"/\\"/g' | sed 's/\\\\/\\\\\\\\/g')"

	echo "{"
	echo "		\"ID\" : $1,"
	echo "		\"Name\" : \"$name\","
	echo "		\"CreationDate\" : \"$3\","
	echo "		\"ModificationDate\" : \"$4\","
	echo "		\"UserName\" : \"$username\","
	echo "		\"Password\" : \"$password\","
	echo "		\"URL\" : \"$url\","
	echo "		\"Notes\" : \"$notes\""
	echo "	}"
}

# Get entry's data and return a CSV representation.
# @param {number} $1 - Entry's id
# @param {string} $2 - Entry's name
# @param {date}   $3 - Entry's creation date
# @param {date}   $4 - Entry's modification date
# @param {string} $5 - Entry's username
# @param {string} $6 - Entry's password
# @param {string} $7 - Entry's URL
# @param {string} $8 - Entry's Notes
function entryToCSV () {
	name="$(echo "$2" | sed 's/"/""/g')"
	username="$(echo "$5" | sed 's/"/""/g')"
	password="$(echo "$6" | sed 's/"/""/g')"
	url="$(echo "$7" | sed 's/"/""/g')"
	notes="$(echo "$8" | sed 's/"/""/g')"
	echo -n "$1,"
	echo -n "\"$name\","
	echo -n "\"$3\","
	echo -n "\"$4\","
	echo -n "\"$username\","
	echo -n "\"$password\","
	echo -n "\"$url\","
	echo "\"$notes\""
}

# Get entry's data and return a SQL representation.
# @param {number} $1 - Entry's id
# @param {string} $2 - Entry's name
# @param {date}   $3 - Entry's creation date
# @param {date}   $4 - Entry's modification date
# @param {string} $5 - Entry's username
# @param {string} $6 - Entry's password
# @param {string} $7 - Entry's URL
# @param {string} $8 - Entry's Notes
function entryToSQL () {
	name="$(echo "$2" | sed "s/'/\\'/g" | sed 's/\\\\/\\\\\\\\/g')"
	username="$(echo "$5" | sed "s/'/\\'/g" | sed 's/\\\\/\\\\\\\\/g')"
	password="$(echo "$6" | sed "s/'/\\'/g" | sed 's/\\\\/\\\\\\\\/g')"
	url="$(echo "$7" | sed "s/'/\\'/g" | sed 's/\\\\/\\\\\\\\/g')"
	notes="$(echo "$8" | sed "s/'/\\'/g" | sed 's/\\\\/\\\\\\\\/g')"

	echo -n "($1,"
	echo -n "'$name',"
	echo -n "'$3',"
	echo -n "'$4',"
	echo -n "'$username',"
	echo -n "'$password',"
	echo -n "'$url',"
	echo "'$notes')"
}

# Get entry's data and return a TEXT representation.
# @param {number} $1 - Entry's id
# @param {string} $2 - Entry's name
# @param {date}   $3 - Entry's creation date
# @param {date}   $4 - Entry's modification date
# @param {string} $5 - Entry's username
# @param {string} $6 - Entry's password
# @param {string} $7 - Entry's URL
# @param {string} $8 - Entry's Notes
function entryToTXT () {
	echo ""
	echo "$1"
	echo "$2"
	echo "$3"
	echo "$4"
	echo "$5"
	echo "$6"
	echo "$7"
	echo "$8"
}

# Break the fields from a CSV line to many lines.
# Adds a dot to the begining of every field so to keep the empty elements when it splitted to an array.
# @@@ Can't handle "" and , inside an entry
# @param {string} $1 - CSV line to process
function CSVBreakRowToLines () {
	echo "$1" | awk 'BEGIN {FPAT = "([^,]*)|([\"][^\"]+[\"])";} {for(i=1; i<=NF; i++) {printf(".%s\n", $i)}}'
}

# Returns the contents of a csv field unescaped.
# @param {string} $1 - The contents of the csv field.
function CSVFieldToVar () {
	f="$1"
	f="${f:1}"
	echo "$f" | sed -r 's/["]([^"])|["]$/\1/g;s/["]{2}/"/g'
}

# Returns the contents of an xml field unescaped.
# @param {string} $1 - The contents of the xml field.
function XMLFieldToVar () {
	f="$1"
	f="${f:1}"
	echo "$f" | xmlstarlet unesc # recode html..ascii

}

# Reads a CSV string and imports entries data to database if they don't already exist.
# @param {string} $1 - The decrypted database file location.
# @param {string} $2 - The CSV data.
function importCSV () {

	dbfile="$1"
	importsrc="$2"

	addedcount="0"

	while read line; do
		sIFS="$IFS"
		IFS=$'\n'
		efields=( $(CSVBreakRowToLines "$line") )
		IFS="$sIFS"
		efields[1]="$(CSVFieldToVar "${efields[1]}")"
		if [ "$(itemExists "$dbfile" "${efields[1]}")" == "false" ]; then
			efields[0]="$(CSVFieldToVar "${efields[0]}")"
			efields[2]="$(CSVFieldToVar "${efields[2]}")"
			efields[3]="$(CSVFieldToVar "${efields[3]}")"
			efields[4]="$(CSVFieldToVar "${efields[4]}")"
			efields[5]="$(CSVFieldToVar "${efields[5]}")"
			efields[6]="$(CSVFieldToVar "${efields[6]}")"
			efields[7]="$(CSVFieldToVar "${efields[7]}")"

			addItem "$dbfile"\
					""\
					"${efields[1]}"\
					"${efields[4]}"\
					"${efields[5]}"\
					"${efields[6]}"\
					"${efields[7]}"

			tstat="$?"


			if [ "$tstat" == "0" ]; then

				echo "Added entry \"${efields[1]}\"" > /dev/tty

				addedcount=$[ "$addedcount" + "1" ]

			else

				echo "Error on adding the entry \"${efields[1]}\". Command returns $tstat code."  > /dev/stderr

			fi

		else

			echo "Entry \"${efields[1]}\" already exists." > /dev/tty

		fi

	done < <(echo "$importsrc" | tail -n +2)

	echo "$addedcount"
}

# Reads a JSON string and imports entries data to database if they don't already exist.
# @param {string} $1 - The decrypted database file location.
# @param {string} $2 - The JSON data.
function importJSON () {
	dbfile="$1"
	importsrc="$2"

	addedcount="0"
	while read i; do

		eID="$(jq -r '.ID' <<< "$i")"
		eName="$(jq -r '.Name' <<< "$i")"
		eUsername="$(jq -r '.UserName' <<< "$i")"
		ePassword="$(jq -r '.Password' <<< "$i")"
		eURL="$(jq -r '.URL' <<< "$i")"
		eNotes="$(jq -r '.Notes' <<< "$i")"

		if [ "$(itemExists "$dbfile" "$eName")" == "false" ]; then

			addItem "$dbfile"\
					""\
					"$eName"\
					"$eUsername"\
					"$ePassword"\
					"$eURL"\
					"$eNotes"

			tstat="$?"

			if [ "$tstat" == "0" ]; then

				echo "Added entry \"$eName\"" > /dev/tty

				addedcount=$[ "$addedcount" + "1" ]

			else

				echo "Error on adding the entry \"$eName\". Command returns $tstat code." > /dev/stderr

			fi

		else

			echo "Entry \"$eName\" already exists." > /dev/tty

		fi


	done < <( echo "$importsrc" | jq -c '.[]')

	echo "$addedcount"
}

# Reads an XML string and imports entries data to database if they don't already exist.
# @param {string} $1 - The decrypted database file location.
# @param {string} $2 - The XML data.
function importXML () {
	dbfile="$1"
	importsrc="$2"

	addedcount="0"
	sep=$'\uffff'

	while IFS="$sep" read -r eID eName eCDate eMDate eUsername ePassword eURL eNotes; do

		eID="$(XMLFieldToVar "$eID")"
		eName="$(XMLFieldToVar "$eName")"
		eCDate="$(XMLFieldToVar "$eCDate")"
		eMDate="$(XMLFieldToVar "$eMDate")"
		eUsername="$(XMLFieldToVar "$eUsername")"
		ePassword="$(XMLFieldToVar "$ePassword")"
		eURL="$(XMLFieldToVar "$eURL")"
		eNotes="$(XMLFieldToVar "$eNotes")"

		if [ "$(itemExists "$dbfile" "$eName")" == "false" ]; then

			addItem "$dbfile"\
					""\
					"$eName"\
					"$eUsername"\
					"$ePassword"\
					"$eURL"\
					"$eNotes"
			tstat="$?"

			if [ "$tstat" == "0" ]; then

				echo "Added entry \"$eName\"" > /dev/tty

				addedcount=$[ "$addedcount" + "1" ]

			else

				echo "Error on adding the entry \"$eName\". Command returns $tstat code." > /dev/stderr

			fi

		else

			echo "Entry \"$eName\" already exists." > /dev/tty

		fi

	done < <(echo "$importsrc" | xmlstarlet sel -t -m '//Data/Entry' \
						-o "." -v ./ID -o "$sep" \
						-o "." -v ./Name -o "$sep" \
						-o "." -v ./CreationDate -o "$sep" \
						-o "." -v ./ModificationDate -o "$sep" \
						-o "." -v ./UserName -o "$sep" \
						-o "." -v ./Password -o "$sep" \
						-o "." -v ./URL -o "$sep" \
						-o "." -v ./Notes -n)

	echo "$addedcount"
}

# Reads an SQL string and imports entries data to databse if they don't already exist.
# @param {string} $1 - The decrypted database file location.
# @param {string} $2 - The SQL data.
function importSQL () {
	dbfile="$1"
	importsrc="$2"

	addedcount="0"

	echo -e "Executing sql..." > /dev/tty

	itemDate="$(date +'%Y-%m-%d %H:%M:%S')"

	tblkeysb="pass_id INTEGER PRIMARY KEY, pass_name TEXT, pass_creation_date TEXT, pass_modification_date TEXT, pass_username TEXT, pass_password TEXT, pass_url TEXT, pass_notes TEXT"
	inssql="CREATE TABLE IF NOT EXISTS passwords1 ( ${tblkeysb} ); CREATE TABLE IF NOT EXISTS passwords2 ( ${tblkeysb} );"
	inssql="${inssql} DELETE FROM passwords1; DELETE FROM passwords2;"
	inssql="${inssql} $(echo "$importsrc" | sed -r 's/(INSERT[ ]+(OR[ ]+REPLACE[ ]+)?INTO[ ]+)([^ ]+)[ ]/\1\31/g')"

	sqlite3 "$dbfile" "${inssql}" > /dev/null

	sep=$'\uffff'

	while IFS="$sep" read -r fname uname passwd purl pnotes; do

		sqlite3 "$dbfile" "UPDATE passwords1 SET pass_name = '$(echo "${fname}" | base64)', pass_username = '$(echo "${uname}" | base64)', pass_password = '$(echo "${passwd}" | base64)', pass_url = '$(echo "${purl}" | base64)', pass_notes = '$(echo "${pnotes}" | base64)'  WHERE pass_name = '${fname}';" > /dev/null

	done < <(sqlite3 -separator $'\uffff' "$dbfile" "SELECT pass_name, pass_username, pass_password, pass_url, pass_notes FROM passwords1")

	tblkeys="NULL, A.pass_name, '${itemDate}', '${itemDate}', A.pass_username, A.pass_password, A.pass_url, A.pass_notes"

	inssql="INSERT INTO passwords ( pass_id, pass_name, pass_creation_date, pass_modification_date, pass_username, pass_password, pass_url, pass_notes ) SELECT ${tblkeys} FROM passwords1 A LEFT JOIN passwords B ON A.pass_name = B.pass_name WHERE B.pass_name IS NULL;"
	inssql="${inssql}; SELECT total_changes();"

	addedcount="$(sqlite3 -csv "$dbfile" "$inssql" | tail -n 1)"

	sqlite3 "$dbfile" "DROP TABLE passwords1; DROP TABLE passwords2" > /dev/null

	echo "$addedcount"
}

# Check if file settings.sh exist and load it if so.
[ -f "/etc/passman/settings.sh" ] && source /etc/passman/settings.sh

if [ -f ~/.local/etc/passman/settings.sh ]; then

	source ~/.local/etc/passman/settings.sh

elif [ -f ~/.local/share/passman/settings.sh ]; then

	source ~/.local/share/passman/settings.sh

fi
# [ -f "./settings.sh" ] &&  source ./settings.sh # thisone is only for debugging purposes

# Read command line arguments

action="nothing"
argact="read arguments"

## Read global arguments and commands
while [ $# -gt 0 ]; do

	key="$1"

	case "$1" in
		--pw|--password)
			shift
			dbpassword="$1"
			shift
			;;

		--pw=*|--password=*)
			dbpassword="${key#*=}"
			shift
			;;

		--db|--database)
			shift
			dbfilepath="$1"
			shift
			;;

		--db=*|--database=*)
			dbfilepath="${key#*=}"
			shift
			;;

		-h|--help)
			argact="none"
			askpasswordifnotinargs="false"
			if [ "$2" == "new" ]; then
				action="new entry help"
			elif [ "$2" == "get" ]; then
				action="get entry help"
			elif [ "$2" == "edit" ]; then
				action="edit entry help"
			elif [ "$2" == "del" ]; then
				action="del entry help"
			elif [ "$2" == "list" ]; then
				action="list entry help"
			elif [ "$2" == "gen" ]; then
				action="gen entry help"
			elif [ "$2" == "export" ]; then
				action="export entry help"
			elif [ "$2" == "clear" ]; then
				action="clear help"
			elif [ "$2" == "import" ]; then
				action="import entry help"
			elif [ "$2" == "encrypt" ]; then
				action="encrypt help"
			elif [ "$2" == "decrypt" ]; then
				action="decrypt help"
			elif [ "$2" == "build" ]; then
				action="build help"
			elif [ "$2" == "backup" ]; then
				action="backup help"
			elif [ "$2" == "setting" ]; then
				action="setting help"
			else
				action="help"
			fi
			break
			shift
			;;

		"test")
			# getListOfBackupsForEntry "$dbfilepath" "$dbpassword" "insomnia.gr"
			exit 0
			;;

		-v|--version)
			action="version"
			argact="none"
			askpasswordifnotinargs="false"
			break
			shift
			;;

		"export")
			argact="read args for export"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		"import")
			argact="read args for import"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		decrypt)
			argact="read args for decrypt"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		encrypt)
			argact="read args for encrypt"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		new|newitem|newpassword)
			argact="read args for newentry"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		get|getitem|getpassword|view)
			argact="read args for getentry"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		edit|modify|edititem|modifyitem|editpassword|modifypassword)
			argact="read args for edititem"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		del|delete|delitem|delpassword)
			argact="read args delitem"
			askpasswordifnotinargs="true"
			shift
			break
			;;
		

		list|listitems|listpasswords)
			argact="read args for listentries"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		gen|generate)
			argact="read args for generate"
			askpasswordifnotinargs="false"
			shift
			break
			;;

		clear)
			argact="read args for clear"
			askpasswordifnotinargs="false"
			shift
			break
			;;

		build)
			argact="read args for build"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		backup)
			argact="read args for backup"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		setting)
			argact="read args for setting"
			askpasswordifnotinargs="true"
			shift
			break
			;;

		*)
			printError "Unknown action \"$key\""
			exit 125
			;;
	esac
	
done

# Read command line arguments for new entry
if [ "$argact" == "read args for newentry" ]; then
	
	action="new entry"
	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="new entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			--yes|-y)
				confirmentry="false"
				shift
				;;

			--item)
				shift
				itemname="$1"
				shift
				;;

			--item=*)
				itemname="${key#*=}"
				shift
				;;

			--item-username|--itmun)
				shift
				itemusername="$1"
				shift
				;;

			--item-username=*|--itmun=*)
				itemusername="${key#*=}"
				shift
				;;

			--item-password|--itmpw)
				shift
				itempassword="$1"
				itemgeneratepass="false"
				shift
				;;

			--item-password=*|--itmpw=*)
				itempassword="${key#*=}"
				itemgeneratepass="false"
				shift
				;;

			--item-passgen)
				itemgeneratepass="true"
				shift
				;;

			--item-url|--itmurl)
				shift
				itemurl="$1"
				shift
				;;

			--item-url=*|--itmurl=*)
				itemurl="${key#*=}"
				shift
				;;

			--item-notes|--itmnt)
				shift
				itemnotes="$1"
				shift
				;;

			--item-notes=*|--itmnt=*)
				itemnotes="${key#*=}"
				shift
				;;

			*)
				printError "Unknown argument \"$key\""
				exit 125
				;;

		esac

	done


# Read command line arguments for getting entry
elif [ "$argact" == "read args for getentry" ]; then
	
	action="get entry"
	argact="read arguments"
	selectColumns=""
	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="get entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			--item)
				shift
				entryname="$1"
				shift
				;;

			--item=*)
				entryname="${key#*=}"
				shift
				;;

			--show-id)
				selectColumns+=",id"
				shift
				;;

			--show-name)
				selectColumns+=",name"
				shift
				;;

			--show-creation-date|--show-cdate)
				selectColumns+=",cdate"
				shift
				;;

			--show-modification-date|--show-mdate)
				selectColumns+=",mdate"
				shift
				;;

			--show-username)
				selectColumns+=",username"
				shift
				;;

			--show-password)
				hidepass="false"
				selectColumns+=",password"
				shift
				;;

			--show-url)
				selectColumns+=",url"
				shift
				;;

			--show-notes)
				selectColumns+=",notes"
				shift
				;;

			--columns=*)
				selectColumns="${key#*=}"
				shift
				;;

			--columns)
				shift
				selectColumns="$1"
				shift
				;;

			--clip=*)
				copytoclipboard="${key#*=}"
				shift
				;;

			*)
				if [ "$#" == "1" ] && [ "$entryname" == "" ]; then

					entryname="$key"
					shift

				else
					printError "Unknown argument \"$key\""
					exit 125

				fi
				;;

		esac

	done

	if [ "$selectColumns" != "" ]; then
		declare -A cselected
		cselected[id]="false"
		cselected[name]="false"
		cselected[cdate]="false"
		cselected[mdate]="false"
		cselected[username]="false"
		cselected[password]="false"
		cselected[url]="false"
		cselected[notes]="false"

		for i in ${selectColumns//,/ }; do
			cselected[$i]="true"
		done

		selectColumns=""

		[ "${cselected[id]}" == "true" ] && selectColumns+=",id"
		[ "${cselected[name]}" == "true" ] && selectColumns+=",name"
		[ "${cselected[cdate]}" == "true" ] && selectColumns+=",cdate"
		[ "${cselected[mdate]}" == "true" ] && selectColumns+=",mdate"
		[ "${cselected[username]}" == "true" ] && selectColumns+=",username"
		[ "${cselected[password]}" == "true" ] && selectColumns+=",password"
		[ "${cselected[url]}" == "true" ] && selectColumns+=",url"
		[ "${cselected[notes]}" == "true" ] && selectColumns+=",notes"

		selectColumns="${selectColumns:1}"

	else

		selectColumns="id,name,cdate,mdate,username,password,url,notes"

	fi

# Read command line arguments for editing entry
elif [ "$argact" == "read args for edititem" ]; then

	action="modify entry"
	argact="read arguments"

	delusername="false"
	delpassword="false"
	delurl="false"
	delnotes="false"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="edit entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			--yes|-y)
				confirmentry="false"
				shift
				;;

			--item)
				shift
				entryname="$1"
				shift
				;;

			--item=*)
				entryname="${key#*=}"
				shift
				;;

			--item-name|--itmnn)
				shift
				itemnewname="$1"
				shift
				;;

			--item-name=*|--itmnn=*)
				itemnewname="${key#*=}"
				shift
				;;

			--item-username|--itmun)
				shift
				itemusername="$1"
				shift
				;;

			--item-username=*|--itmun=*)
				itemusername="${key#*=}"
				shift
				;;

			--item-password|--itmpw)
				shift
				itempassword="$1"
				itemgeneratepass="false"
				shift
				;;

			--item-password=*|--itmpw=*)
				itempassword="${key#*=}"
				itemgeneratepass="false"
				shift
				;;

			--item-passgen)
				itemgeneratepass="true"
				shift
				;;

			--item-url|--itmurl)
				shift
				itemurl="$1"
				shift
				;;

			--item-url=*|--itmurl=*)
				itemurl="${key#*=}"
				shift
				;;

			--item-notes|--itmnt)
				shift
				itemnotes="$1"
				shift
				;;

			--item-notes=*|--itmnt=*)
				itemnotes="${key#*=}"
				shift
				;;

			--delete-username|--dun)
				delusername="true"
				shift
				;;

			--delete-password|--dpw)
				delpassword="true"
				shift
				;;

			--delete-url|--dul)
				delurl="true"
				shift
				;;

			--delete-notes|--dnt)
				delnotes="true"
				shift
				;;

			*)
				printError "Unknown argument \"$key\""
				exit 125
				;;

		esac

	done

# Read command line arguments for deleting entries
elif [ "$argact" == "read args delitem" ]; then

	action="delete entry"
	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="del entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			--yes|-y)
				confirmentry="false"
				shift
				;;

			--item)
				shift
				entryname="$1"
				shift
				;;

			--item=*)
				entryname="${key#*=}"
				shift
				;;

			*)
				printError "Unknown argument \"$key\""
				exit 125
				;;

		esac

	done

# Read command line arguments for listing entries
elif [ "$argact" == "read args for listentries" ]; then

	action="get entries list"
	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="list entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			-b|--begin)
				shift
				beginitem="$1"
				shift
				;;

			-b=*|--begin=*)
				beginitem="${key#*=}"
				shift
				;;

			-l|--length)
				shift
				listlength="$1"
				shift
				;;

			-l=*|--length=*)
				listlength="${key#*=}"
				shift
				;;

			*)
				printError "Unknown argument \"$key\""
				exit 125
				;;
		esac

	done

# Read command line arguments for generating password
elif [ "$argact" == "read args for generate" ]; then

	action="generate password"
	argacgt="read arguments"
	commandstr=""

	if [ -f "${passgencmd}" ]; then

		while [[ $# -gt 0 ]]; do
			commandstr+=" $1"
			shift
		done

		commandstr="${passgencmd} ${commandstr}"

		$commandstr

		exit $?

	else

		printError "The script \"passgen\" cannot be found on \"${passgencmd}\". Try setting the location of the script in \"settings.sh\"."

	fi

# Read command line arguments for clearing traces
elif [ "$argact" == "read args for clear" ]; then

	action="clear traces"
	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="clear help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					--tmpdb=*)
						tmpdbfilename="${key#*=}"
						[ -f "$tmpdbfilename" ] && rm "$tmpdbfilename"
						[ -f "$tmpdbsfilename" ] && rm "$tmpdbsfilename"
						[ -f "$tmpdbpfilename" ] && rm "$tmpdbpfilename"
						shift
						;;

					--clipboard=*)
						export DISPLAY=:0.0
						tmpfile="${key#*=}"
						ppass="$(clippaste 2>/dev/null)"
						if [ -f "$tmpfile" ]; then
							mpass="$(cat "$tmpfile")"
							rm "$tmpfile"

							[ "$ppass" == "$mpass" ] && clipcopy " "
						fi
						shift
						;;

					--history)
						hfile="$(realpath ~/.bash_history)"
						hist=($(cat -n $hfile | egrep -E "passman.*--(password|pw|itmpw|item-password)" | sed -r 's/^\s*([0-9]+)\s*.*/\1/g' | tac))
						for i in ${hist[@]}; do
							sed -i "${i}d" $hfile
						done
						history -r
						echo "You may have to run \"history -r\" for changes to take effect."
						shift
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;

				esac
				;;

		esac

	done

elif [ "$argact" == "read args for export" ]; then

	action="export"

	argact="read arguments"

	exporttype="none"
	exportoverwrite="false"
	databegin="-"
	datalength="-"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="export entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			-b|--begin)
				shift
				databegin="$1"
				shift
				;;

			-b=*|--begin=*)
				databegin="${key#*=}"
				shift
				;;

			-l|--length)
				shift
				datalength="$1"
				shift
				;;

			-l=*|--length=*)
				datalength="${key#*=}"
				shift
				;;

			-f|--format)
				shift
				exporttype="$1"
				shift
				;;

			-f=*|--format=*)
				exporttype="${key#*=}"
				shift
				;;

			--overwrite)
				exportoverwrite="true"
				shift
				;;

			*)
				if [ "$#" == "1" ] && [ "$exportpath" == "" ]; then

					exportopath="$key"
					exportpath="$(realpath "$key" 2> /dev/null)"
					[ "$?" == "0" ] && exportpathexists="true" || exportpathexists="false"
					exportfilename="$(basename "$exportpath")"
					exportdirectory="$(dirname "$exportpath")"
					if [ "$exportpathexists" == "false" ]; then
						printError "Export directory does not exist or is inaccessible"
						exit 121
					fi
					shift

				else
					printError "Unknown argument \"$key\""
					exit 125

				fi
				;;

		esac

	done

elif [ "$argact" == "read args for import" ]; then

	action="import"

	argact="read arguments"

	importsourcetype="none"
	importtype="detect"
	importfile=""

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$key" in

			--help|-h)
				action="import entry help"
				argact="none"
				askpasswordifnotinargs="false"
				break
				;;

			-f|--file)
				importsourcetype="file"
				shift
				importfile="$1"
				shift
				;;

			-f=*|--file=*)
				importsourcetype="file"
				importfile="${key#*=}"
				shift
				;;

			-t|--type)
				shift
				importtype="$1"
				shift
				;;

			-t=*|--type=*)
				importtype="${key#*=}"
				shift
				;;

			--|--stdin)
				importsourcetype="stdin"
				shift
				;;

			*)
				printError "Unknown argument \"$key\""
				exit 125
				;;

		esac

	done

elif [ "$argact" == "read args for encrypt" ]; then

	action="encrypt database"

	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="encrypt help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;
				esac
				;;

		esac

	done

elif [ "$argact" == "read args for decrypt" ]; then

	action="decrypt database"

	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="decrypt help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;
				esac
				;;

		esac

	done

elif [ "$argact" == "read args for build" ]; then

	action="build"

	argact="read arguments"

	while [[ $# -gt 0 ]]; do
		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="build help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;
				esac
				;;

		esac

	done

elif [ "$argact" == "read args for backup" ]; then

	action="backup"

	argact="read arguments"

	confirmrestore="true"

	while [[ $# -gt 0 ]]; do

		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="backup help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					--entry=*)
						entryname="${key#*=}"
						shift
						;;

					--entry)
						argact="get name of entry"
						shift
						;;

					--list=*)
						restoreitem="${key#*=}"
						backupaction="list"
						shift
						;;

					--list)
						backupaction="list"
						argact="get item to restore"
						shift
						;;

					--restore=*)
						restoreitem="${key#*=}"
						backupaction="restore"
						shift
						;;

					--restore)
						backupaction="restore"
						argact="get item to restore"
						shift
						;;

					--view=*)
						restoreitem="${key#*=}"
						backupaction="view entry"
						shift
						;;

					--view)
						backupaction="view entry"
						argact="get item to restore"
						shift
						;;

					--reveal-password|-rp)
						hidepass="false"
						shift
						;;

					--yes|-y)
						confirmrestore="false"
						shift
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;

				esac
				;;

			"get name of entry")
				entryname="${key}"
				argact="read arguments"
				shift
				;;

			"get item to restore")
				restoreitem="${key#*=}"
				argact="read arguments"
				shift
				;;

		esac

	done

elif [ "$argact" == "read args for setting" ]; then

	action="setting"
	argact="read arguments"

	while [[ $# -gt 0 ]]; do

		key="$1"

		case "$argact" in

			"read arguments")

				case "$key" in

					-h|--help)
						action="setting help"
						argact="none"
						askpasswordifnotinargs="false"
						break
						;;

					--name=*)
						settingname="${key#*=}"
						shift
						;;

					--name)
						shift
						settingname="$1"
						shift
						;;

					--delete)
						settingaction="delete"
						shift
						;;

					--set=*)
						settingaction="set"
						settingvalue="${key#*=}"
						shift
						;;

					--set)
						settingaction="set"
						shift
						settingvalue="$1"
						shift
						;;

					--get=*)
						settingaction="get"
						settingname="${key#*=}"
						shift
						;;

					--get)
						settingaction="get"
						shift
						settingname="$1"
						shift
						;;

					--list)
						settingaction="list"
						shift
						;;

					*)
						printError "Unknown argument \"$key\""
						exit 125
						;;

				esac
				;;

		esac

	done

fi

# / Read command line arguments

# If database password is not set ask from user
if [ ! -f "$tmpdbsfilename" ] && [ "$askpasswordifnotinargs" == "true" ] && [ -z ${dbpassword+x} ]; then
	read -s -r -p "Password for database \"$dbfilepath\":" dbpassword < /dev/tty
	echo ""
fi

if [ "$action" == "nothing" ]; then

	printError "You must select an action"
	exit 125

################
# Add a new entry
elif [ "$action" == "new entry" ]; then

	# If item name is not set, ask from user
	if [ -z ${itemname+x} ]; then
		read -p "Type a name for the entry: " itemname
	fi

	# If item username is not set, ask it from user
	if [ -z ${itemusername+x} ]; then
		read -p "Type a username for the entry: " itemusername
	fi

	# If item's password is not set and there is no option for generating it, ask it from user
	if [ "$itemgeneratepass" != "true" ] && [ -z ${itempassword+x} ]; then

		while true; do
			read -s -r -p "Type a password for the entry: " itempassword
			echo ""

			read -s -r -p "Type the password again: " itempassword2
			echo ""

			if [ "$itempassword" != "$itempassword2" ]; then
				echo "Passwords don't match, try again"
			else
				break
			fi
		done

	fi

	# If item's url is not set, ask it from user
	if [ -z ${itemurl+x} ]; then
		read -p "Type a url for the entry: " itemurl
	fi

	# If item's notes is not set, ask it from user
	if [ -z ${itemnotes+x} ]; then
		read -p "Type some notes for the entry: " itemnotes
	fi

	if [ "$confirmentry" == "true" ]; then
		echo ""
		echo "Please confirm entry data:"
		echo "name: $itemname"
		echo "username: $itemusername"
		#echo "password: $itempassword $itemgeneratepass"
		echo "url: $itemurl"
		echo "notes: $itemnotes"
		echo ""

		while true; do
			read -p "Is the data correct? (Yes/No)" confirmdata

			if [[ "$confirmdata" =~ ^([Yy]([Ee][Ss])?|[Nn][Oo]?)$ ]]; then
				break
			else
				echo 'You must type "Yes" or "No".'
			fi
		done
	else
		confirmdata="yes"
	fi

	if [[ "$confirmdata" =~ ^[Yy]([Ee][Ss])?$ ]]; then
		echo "Saving entry . . . "

		addItem "$dbfilepath"\
				"$dbpassword"\
				"$itemname"\
				"$itemusername"\
				"$itempassword"\
				"$itemurl"\
				"$itemnotes"

		[ "$?" == "0" ] &&  echo "Entry saved!" && dbchanged="true" || printError "Error"

	elif [[ "$confirmdata" =~ ^[Nn][Oo]?$ ]]; then
		echo "Abord"
	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	## Clear data before exit
	clearDataOnExit

################
# Get data from a single entry
elif [ "$action" == "get entry" ]; then

	# If password and database file are set then
	# decrypt database to a temporary file
	#if [ ! -z ${dbfilepath+x} ] && [ ! -z ${dbpassword+x} ]; then
	#	dbfilepathtmp="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"
	#fi

	if [ "$copytoclipboard" != "false" ]; then

		tmpdbfilename="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"
		entrydata="$(getItemsEntry "$tmpdbfilename" "" "$entryname" "$copytoclipboard")"

		clipcopy "$entrydata"

		if [ "$clearafter" != "false" ]; then

			tmpfilename="$(mktemp "/tmp/passman.XXXXXXX")"
			echo -e "$entrydata" > $tmpfilename
			echo "$0 clear --clipboard=\"$tmpfilename\"" | at now + "$clearafter" minute 2> /dev/null

		fi

	else

		getItem "$dbfilepath" "$dbpassword" "$entryname" "$selectColumns" "$hidepass"

	fi

	## Clear data before exit
	clearDataOnExit

################
# Modify an entry
elif [ "$action" == "modify entry" ]; then

	# If item name is not set, ask from user
	if [ -z ${entryname+x} ]; then
		read -p "Type the name of the entry to modify: " entryname
	fi

	# If item name is not set, ask from user
	if [ -z ${itemnewname+x} ]; then
		read -p "Type the new name for the entry or hit enter to leave the previous value: " itemnewname
		itemnewname=${itemnewname:-$entryname}
	fi

	# If item username is not set, ask it from user
	if [ "$delusername" == "false" ] && [ -z ${itemusername+x} ]; then
		read -p "Type the new username for the entry or hit enter for leaving it unchanged: " itemusername
		[ "$itemusername" == "" ] && unset itemusername
	fi

	# If item's password is not set and there is no option for generating it, ask it from user
	if [ "$itemgeneratepass" != "true" ] && [ "$delpassword" == "false" ] && [ -z ${itempassword+x} ]; then

		while true; do
			read -s -r -p "Type the new password for the entry or hit enter to skip: " itempassword
			echo ""

			if [ "$itempassword" == "" ]; then
				unset itempassword
				break
			fi

			read -s -r -p "Type the password again: " itempassword2
			echo ""

			if [ "$itempassword" != "$itempassword2" ]; then
				echo "Passwords don't match, try again"
			else
				break
			fi
		done
	fi

	# If item's url is not set, ask it from user
	if [ "$delurl" == "false" ] && [ -z ${itemurl+x} ]; then
		read -p "Type the new url for the entry or hit enter to skip: " itemurl
		[ "$itemurl" == "" ] && unset itemurl
	fi

	# If item's notes is not set, ask it from user
	if [ "$delnotes" == "false" ] && [ -z ${itemnotes+x} ]; then
		read -p "Type some notes for the entry or hit enter to skip: " itemnotes
		[ "$itemnotes" == "" ] && unset itemnotes
	fi

	args=""
	[ ! -z ${itemnewname+x}  ] && args=",name"
	[ ! -z ${itemusername+x} ] && args+=",username"
	[ ! -z ${itempassword+x} ] && args+=",password"
	[ ! -z ${itemurl+x} ]      && args+=",url"
	[ ! -z ${itemnotes+x} ]    && args+=",notes"

	if [ "$confirmentry" == "true" ]; then
		echo ""
		echo "Please confirm entry data:"
		echo "Entry to be modified: $entryname"
		[ ! -z ${itemusername+x} ] && echo "New username: $itemusername"
		 [ ! -z ${itempassword+x} ] && echo "New password: $itempassword"
		[ ! -z ${itemurl+x} ] && echo "New url: $itemurl"
		[ ! -z ${itemnotes+x} ] && echo "New notes: $itemnotes"
		echo ""

		while true; do
			read -p "Is the data correct? (Yes/No)" confirmdata < /dev/tty

			if [[ "$confirmdata" =~ ^([Yy]([Ee][Ss])?|[Nn][Oo]?)$ ]]; then
				break
			else
				echo 'You must type "Yes" or "No".'
			fi
		done
	else
		confirmdata="yes"
	fi

	if [[ "$confirmdata" =~ ^[Yy]([Ee][Ss])?$ ]]; then
		echo "Saving entry . . . "

		editItem "$dbfilepath"\
				 "$dbpassword"\
				 "$entryname"\
				 "${args:1}"\
				 "$itemnewname"\
				 "$itemusername"\
				 "$itempassword"\
				 "$itemurl"\
				 "$itemnotes"\
				 "${delusername},${delpassword},${delurl},${delnotes}"

		[ "$?" == "0" ] && echo "Entry saved!" && dbchanged="true" || printError "Error"
	elif [[ "$confirmdata" =~ ^[Nn][Oo]$ ]]; then
		echo "Abord"
	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	## Clear data before exit
	clearDataOnExit


################
# Delete an entry
elif [ "$action" == "delete entry" ]; then

	if [ "$confirmentry" == "true" ]; then
		echo ""
		echo "Please confirm entry data:"
		echo "Entry to be deleted: $entryname"
		echo ""

		while true; do
			read -p "Is the data correct? (Yes/No)" confirmdata < /dev/tty

			if [[ "$confirmdata" =~ ^([Yy]([Ee][Ss])?|[Nn][Oo]?)$ ]]; then
				break
			else
				echo 'You must type "Yes" or "No".'
			fi
		done
	else
		confirmdata="yes"
	fi

	if [[ "$confirmdata" =~ ^[Yy]([Ee][Ss])?$ ]]; then
		echo "Deleting entry . . . "

		deleteItem "$dbfilepath"\
				   "$dbpassword"\
				   "$entryname"


		[ "$?" == "0" ] && echo "Entry deleted" && dbchanged="true" || printError "Error"

	elif [[ "$confirmdata" =~ ^[Nn][Oo]?$ ]]; then
		echo "Abord"
	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	## Clear data before exit
	clearDataOnExit

################
# Get list of entries
elif [ "$action" == "get entries list" ]; then

	getListOfItems "$dbfilepath" "$dbpassword" "$beginitem" "$listlength"

	## Clear data before exit
	clearDataOnExit

################
# Export data
elif [ "$action" == "export" ]; then

	#echo "$databegin"
	#echo "$datalength"
	#echo "$exporttype"

	data="$(getListOfItemsB64 "$dbfilepath" "$dbpassword" "$databegin" "$datalength")"

	# 11|eWFob28uZ3IK|2021-11-03 21:32:42|2021-11-03 21:32:42|ZWx2aXpha29zCg==|MTIzNDU2Cg==|aHR0cHM6Ly93d3cueWFob28uZ3IK|
	# 22|aW5zb21uaWEuZ3IK|2021-11-03 21:32:44|2021-11-03 21:32:44|ZWx2aXpha29zCg==|MTIzNDU2Cg==|aHR0cHM6Ly93d3cuaW5zb21uaWEuZ3IK|

	# pass_id
	# pass_name
	# pass_creation_date
	# pass_modification_date
	# pass_username
	# pass_password
	# pass_url
	# pass_notes


 #1. sqlite
 #2. xml DONE
 #3. csv
 #4. json DONE
 #5. text | txt

	[ "$exportpath" != "" ] && out="$exportopath" || out="/dev/stdout"

	if [ -f "$out" ] && [ "$out" != "/dev/stdout" ]; then

		if [ "$exportoverwrite" == "false" ]; then

			while true; do
				read -p "Output file exists. Overwrite file? (Yes/No)" confirmoverwrite

				if [[ "$confirmoverwrite" =~ ^([Yy]([Ee][Ss])?|[Nn][Oo]?)$ ]]; then
					break
				else
					echo 'You must type "Yes" or "No".'
				fi
			done

			# if [[ "$confirmoverwrite" =~ ^[Yy]([Ee][Ss])?$ ]]; then

			if [[ "$confirmoverwrite" =~ ^[Nn][Oo]?$ ]]; then
				echo "Abording . . ."
				exit 0
			fi

		fi

	fi

	if [ "$exporttype" == "xml" ]; then
		echo "<?xml version=\"1.0\" ?>" > "$out"
		echo "<Data>" >> "$out"

	elif [ "$exporttype" == "json" ]; then
		echo "[" > "$out"
	elif [ "$exporttype" == "sql" ]; then
		echo "INSERT INTO passwords ( pass_id, pass_name, pass_creation_date, pass_modification_date, pass_username, pass_password, pass_url, pass_notes ) VALUES " > "$out"
	elif [ "$exporttype" == "csv" ]; then
		echo "ID,NAME,\"CREATION DATE\",\"MODIFICATION DATE\",USERNAME,PASSWORD,URL,NOTES" > "$out"
	fi

	while read -r line; do

		if [ "$exporttype" == "json" ] || [ "$exporttype" == "sql" ]; then
			[ "$linearray" != "" ] && echo -n "	," >> "$out" || echo -n "	" >> "$out"
		fi

		IFS='|' read -r -a linearray <<< "$line"

		case "$exporttype" in

			"sql")
				entryToSQL "${linearray[0]}"\
						   "$(echo "${linearray[1]}" | base64 -d)"\
						   "${linearray[2]}"\
						   "${linearray[3]}"\
						   "$(echo "${linearray[4]}" | base64 -d)"\
						   "$(echo "${linearray[5]}" | base64 -d)"\
						   "$(echo "${linearray[6]}" | base64 -d)"\
						   "$(echo "${linearray[7]}" | base64 -d)" >> "$out"
				;;

			"xml")
				entryToXML "${linearray[0]}"\
						   "$(echo "${linearray[1]}" | base64 -d)"\
						   "${linearray[2]}"\
						   "${linearray[3]}"\
						   "$(echo "${linearray[4]}" | base64 -d)"\
						   "$(echo "${linearray[5]}" | base64 -d)"\
						   "$(echo "${linearray[6]}" | base64 -d)"\
						   "$(echo "${linearray[7]}" | base64 -d)" >> "$out"
				;;

			"json")
				entryToJSON "${linearray[0]}"\
						   "$(echo "${linearray[1]}" | base64 -d)"\
						   "${linearray[2]}"\
						   "${linearray[3]}"\
						   "$(echo "${linearray[4]}" | base64 -d)"\
						   "$(echo "${linearray[5]}" | base64 -d)"\
						   "$(echo "${linearray[6]}" | base64 -d)"\
						   "$(echo "${linearray[7]}" | base64 -d)" >> "$out"
				;;

			"csv")
				entryToCSV "${linearray[0]}"\
						   "$(echo "${linearray[1]}" | base64 -d)"\
						   "${linearray[2]}"\
						   "${linearray[3]}"\
						   "$(echo "${linearray[4]}" | base64 -d)"\
						   "$(echo "${linearray[5]}" | base64 -d)"\
						   "$(echo "${linearray[6]}" | base64 -d)"\
						   "$(echo "${linearray[7]}" | base64 -d)" >> "$out"
				;;

			"txt"|"text")
				entryToTXT "${linearray[0]}"\
						   "$(echo "${linearray[1]}" | base64 -d)"\
						   "${linearray[2]}"\
						   "${linearray[3]}"\
						   "$(echo "${linearray[4]}" | base64 -d)"\
						   "$(echo "${linearray[5]}" | base64 -d)"\
						   "$(echo "${linearray[6]}" | base64 -d)"\
						   "$(echo "${linearray[7]}" | base64 -d)" >> "$out"
				;;

		esac

	done < <(echo "$data")

	if [ "$exporttype" == "xml" ]; then
		echo "</Data>" >> "$out"
	elif [ "$exporttype" == "json" ]; then
		echo "]" >> "$out"
	elif [ "$exporttype" == "sql" ]; then
		echo ";" >> "$out"
	fi
	exit 0

################
# Encrypt database
elif [ "$action" == "encrypt database" ]; then

	encryptDB "$dbfilepath" "$dbpassword"

	exit 0

################
# Decrypt database
elif [ "$action" == "decrypt database" ]; then

	decryptDB "$dbfilepath" "$dbpassword"

	exit 0

################
# Import data
elif [ "$action" == "import" ]; then

	addedcount="0"

	if [ "$importsourcetype" == "file" ]; then

		# file -b --mime-type image.png
		# echo "$importfile"
		# echo "$importsourcetype"
		# echo "$importtype"
		# cls; ./passman --password=1234 --db=./passman.db import -t=ssd -f=./import/import.txtt
		# cls; ./passman --password=1234 --db=./passman.db import -t=ssd -f=./import/import.txt


		if [ "$importtype" == "detect" ]; then
			filemimetype="$(file -b --mime-type "$importfile")"

			case "$filemimetype" in

				"application/json")
					importtype="json"
					;;

				"application/csv")
					importtype="csv"
					;;

				"text/xml"|"application/xml")
					importtype="xml"
					;;

				"text/plain")
					filecontents="$(cat "$importfile" | sed -r 's/^\\s*//')"
					if [ "${filecontents:0:22}" == "INSERT INTO passwords " ] ; then
						importtype="sql"
					else
						printError "Unknown or unsupported file mime type."
						exit 120
					fi
					;;

				*)
					printError "Unknown or unsupported file mime type."
					exit 120
					;;

			esac

		fi

		if [ '!' -f "$importfile" ]; then

			printError "File \"$importfile\" does not exists"

			exit 122

		fi

		if [ "$importtype" != "sql" ] && \
			   [ "$importtype" != "xml" ] && \
			   [ "$importtype" != "json" ] && \
			   [ "$importtype" != "csv" ]; then

			printError "Unknown or unsupported file mime type."

			exit 120

		fi


		tmpdbfilename="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"

		cmdstat="$?"

		if [ "$cmdstat" == "0" ]; then

			case "$importtype" in

				"sql")
					addedcount="$(importSQL "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"xml")
					addedcount="$(importXML "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"json")
					addedcount="$(importJSON "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"csv")
					addedcount="$(importCSV "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				*)
					clearDataOnExit
					## Clear data before exit
					printError "Unknown or unsupported file mime type."
					exit 120
					;;

			esac
		fi

	elif [ "$importsourcetype" == "stdin" ]; then

		importstdin=`cat`
		importfile="$(mktemp "/tmp/passmanin.XXXXXX")"
		echo "$importstdin" > $importfile

		if [ "$importtype" == "detect" ]; then
			filemimetype="$(file -b --mime-type "$importfile")"

			case "$filemimetype" in

				"application/json")
					importtype="json"
					;;

				"application/csv")
					importtype="csv"
					;;

				"text/xml"|"application/xml")
					importtype="xml"
					;;

				"text/plain")
					filecontents="$(cat "$importfile" | sed -r 's/^\\s*//')"
					if [ "${filecontents:0:22}" == "INSERT INTO passwords " ] ; then
						importtype="sql"
					else
						printError "Unknown or unsupported file mime type."
						rm -f "$importfile"
						exit 120
					fi
					;;

				*)
					printError "Unknown or unsupported file mime type."
					rm -f "$importfile"
					exit 120
					;;

			esac

		fi

		tmpdbfilename="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"
		cmdstat="$?"

		if [ "$cmdstat" == "0" ]; then

			case "$importtype" in

				"sql")
					addedcount="$(importSQL "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"xml")
					addedcount="$(importXML "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"json")
					addedcount="$(importJSON "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				"csv")
					addedcount="$(importCSV "$tmpdbfilename" "$(cat "$importfile")")"
					;;

				*)
					## Clear data before exit
					clearDataOnExit
					rm -f "$importfile"

					printError "Unknown or unsupported file mime type."
					exit 120
					;;

			esac
		fi

		rm -f "$importfile"
	else

		printError "Data source is not given or can't be found."

		## Clear data before exit
		clearDataOnExit

		exit 122

	fi

	if [ "$addedcount" -gt "0" ]; then

		if [ "$?" == "0" ]; then

			dbchanged="true"
			echo "Added $addedcount new entries"

		else
			echo "Error on restoring database"

		fi

	else

		echo "No new entries added"

	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	## Clear data before exit
	clearDataOnExit

################
# Build database
elif [ "$action" == "build" ]; then

	dbpath="$(createDatabase "$dbfilepath" "$dbpassword")"
	errCode="$?"

	if [ "$errCode" == "0" ]; then
		setSetting "$dbpath" "$dbpassword" "keep_backup_on_edit_entry" "true"
		setSetting "$dbpath" "$dbpassword" "keep_backup_on_delete_entry" "true"
		setSetting "$dbpath" "$dbpassword" "number_of_backup_items_per_entry" "0" # Zero is unlimited
		setSetting "$dbpath" "$dbpassword" "keep_db_unencrypted_for_minutes" "0" # Zero for not keeping db unencrypted

		encryptDB "$dbpath" "$dbpassword"
	fi

################
# Work with backups
elif [ "$action" == "backup" ]; then

	tmpdbfilename="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"

	if [ "$entryname" == "" ]; then
		printError "Name of the entry cannot be empty"
		clearDataOnExit
		exit 1

	else

		if [ "$backupaction" == "list" ]; then

			echo "List of backups for entry \"$entryname\":"
			getListOfBackupsForEntry "$tmpdbfilename" "$dbpassword" "$entryname"

		elif [ "$backupaction" == "view entry" ]; then

			echo "Show backup data of item \"$restoreitem\" for the entry \"$entryname\":"
			viewBackupEntryData "$tmpdbfilename" "$dbpassword" "$entryname" "$restoreitem" "$hidepass"

		elif [ "$backupaction" == "restore" ]; then





			if [ "$confirmrestore" == "true" ]; then
				echo ""
				echo "Please confirm the data to be restored:"

				viewBackupEntryData "$tmpdbfilename" "$dbpassword" "$entryname" "$restoreitem" "false"
				echo ""

				while true; do
					read -p "Is the data correct? (Yes/No)" confirmdata < /dev/tty

					if [[ "$confirmdata" =~ ^([Yy]([Ee][Ss])?|[Nn][Oo]?)$ ]]; then
						break
					else
						echo 'You must type "Yes" or "No".'
					fi
				done
			else
				confirmdata="yes"
			fi

			if [[ "$confirmdata" =~ ^[Yy]([Ee][Ss])?$ ]]; then

				restoreBackup "$tmpdbfilename" "$dbpassword" "$entryname" "$restoreitem"
				encryptDB "$tmpdbfilename" "$dbpassword"

				if [ "$?" == "0" ]; then

					dbchanged="true"
					echo "$entryname restored to $restoreitem backup item."

				else

					printError "Error on restoring entry $entryname. Abording"
					clearDataOnExit
					exit 1

				fi

			fi

		fi

	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	## Clear data before exit
	clearDataOnExit

################
# Work with setting in database
elif [ "$action" == "setting" ]; then

	tmpdbfilename="$(tmpDecryptDB "$dbfilepath" "$dbpassword")"

	if [ "$settingaction" == "" ]; then
		printError "Nothing to do with settings. Exiting."
		clearDataOnExit
		exit 1

	elif [ "$settingaction" == "set" ]; then

		if [ "$settingname" == "" ]; then
			printError "Setting name cannot be empty"
			clearDataOnExit
			exit 1
		fi

		echo "Setting value for \"$settingname\":"
		setSetting "$tmpdbfilename" "$dbpassword" "$settingname" "$settingvalue"
		[ "$?" == "0" ] && echo " Done" && dbchanged="true" || printError "Unknown error"

	elif [ "$settingaction" == "get" ]; then

		if [ "$settingname" == "" ]; then
			printError "Setting name cannot be empty"
			clearDataOnExit
			exit 1
		fi

		# echo "Setting value for \"$settingname\" is : $(getSetting "$tmpdbfilename" "$dbpassword" "$settingname" )"
		echo "$(getSetting "$tmpdbfilename" "$dbpassword" "$settingname" )"

	elif [ "$settingaction" == "delete" ]; then

		if [ "$settingname" == "" ]; then
			printError "Setting name cannot be empty"
			clearDataOnExit
			exit 1
		fi

		echo "Deleting setting \"$settingname\":"
		delSetting "$tmpdbfilename" "$dbpassword" "$settingname"
		[ "$?" == "0" ] && echo " Done" && dbchanged="true" || printError "Unknown error"

	elif [ "$settingaction" == "list" ]; then

		echo "Settings:"
		listSettings "$tmpdbfilename" "$dbpassword"

	fi

	if [ "$dbchanged" == "true" ]; then
		[ -f "$tmpdbsfilename" ] && dbpassword="$(cat "$tmpdbpfilename")"
		cp "$tmpdbfilename" "$dbfilepath"
		encryptDB "$dbfilepath" "$dbpassword"
	fi

	clearDataOnExit
	exit 0

################
# Print help
elif [ "$action" == "help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] COMMAND [ARGUMENTS]"
	echo -e "       \e[1m$(basename $0)\e[m -h|--help|-v|--version"
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mCOMMANDS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[17GThis screen."
	echo -e "  \e[1m-v|--version\e[m\e[17GShows program version."
	echo -e "  \e[1mnew\e[m\e[17GCreates a new entry."
	echo -e "  \e[1mget\e[m\e[17GShows an entry to the screen."
	echo -e "  \e[1medit\e[m\e[17GEdits the data of an entry."
	echo -e "  \e[1mdel\e[m\e[17GDeletes an entry."
	echo -e "  \e[1mlist\e[m\e[17GShows all or a range of the entries in the database."
	echo -e "  \e[1mgen\e[m\e[17GRuns \e[1mpassgen\e[m script. All following arguments are passed to this script."
	echo -e "  \e[1mclear\e[m\e[17GClears passwords from the clipboard and bash history."
	echo -e "  \e[1mdecrypt\e[m\e[17GDecrypts the selected database."
	echo -e "  \e[1mencrypt\e[m\e[17GEncrypts the selected database."
	echo -e "  \e[1mimport\e[m\e[17GImports entries from a file or stdin to the database."
	echo -e "  \e[1mexport\e[m\e[17GExports stored entries to a specified format."
	echo -e "  \e[1mbuild\e[m\e[17GCreates a database file to be used to store passwords."
	echo -e "  \e[1mbackup\e[m\e[17GList of backups of an entry, restore a backup or view backup data."
	echo -e "  \e[1msetting\e[m\e[17GGet list of settings in database and change their values."
	echo ""
	echo -e "\e[1;4mEXIT CODES:\e[m"
	echo -e "  \e[1m120\e[m\e[17GImported file type is not supported."
	echo -e "  \e[1m121\e[m\e[17GDatabase file does not exist or is inaccessible."
	echo -e "  \e[1m122\e[m\e[17GFor \e[1mnew\e[m command, entry already exists. For \e[1medit\e[m and \e[1mdelete\e[m commands, entry does not exist."
	echo -e "  \e[1m123\e[m\e[17GDependency does not exist."
	echo -e "  \e[1m124\e[m\e[17GWrong password for database."
	echo -e "  \e[1m125\e[m\e[17GUnknown command or argument."

################
# Print help for new entry subcommand
elif [ "$action" == "new entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mnew\e[m [ARGUMENTS]"
	echo ""
	echo -e "Create a new entry."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ingored."
	echo -e "  \e[1m-y|--yes\e[m\e[20GDon't ask for confirmation."
	echo -e "  \e[1m--item[=]<name>\e[m\e[20GSets the \e[4m<name>\e[m of the entry."
    echo -e "  \e[1m--itmun[=]<username>\e[m|\e[1m--item-username[=]<username>\e[m"
	echo -e "\e[20GSets the \e[4m<username>\e[m of the entry."
	echo -e "  \e[1m--itmpw[=]<password>\e[m|\e[1m--item-password[=]<password>\e[m"
	echo -e "\e[20GSets the \e[4m<password>\e[m of the entry."
	echo -e "\e[20G\e[1mWARNING:\e[m This will expose the password to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password."
	echo -e "  \e[1m--itmurl[=]<url>\e[m|\e[1m--item-url[=]<url>\e[m"
	echo -e "\e[20GSets the \e[4m<url>\e[m of the entry."
	echo -e "  \e[1m--itmnt[=]<notes>\e[m|\e[1m--item-notes[=]<notes>\e[m"
	echo -e "\e[20GSets some \e[4m<notes>\e[m about the entry."
	echo ""

################
# Print help for get entry subcommand
elif [ "$action" == "get entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mget\e[m [ARGUMENTS]"
	echo ""
	echo -e "Get data from an entry in database."
	echo -e "\e[1;31mAll data will be displayed in plain text\e[m."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ingored."
	echo -e "  \e[1m--item[=]<name>\e[m\e[20GThe \e[4m<name>\e[m of the entry to be displayed."
	echo -e "  \e[1m--clip=<field>\e[m\e[20GCopy a field from an entry to clipboard. Don't show anything to the screen."
	echo -e "\e[20GValid field names are: \e[4mname\e[m, \e[4musername\e[m, \e[4mpassword\e[m, \e[4murl\e[m and \e[4mnotes\e[m."
	echo -e "  \e[1m--show-id\e[m\e[20GShow entry's ID in the database."
	echo -e "  \e[1m--show-name\e[m\e[20GShow entry's name."
	echo -e "  \e[1m--show-cdate\e[m\e[20GShow entry's creation date and time."
	echo -e "  \e[1m--show-mdate\e[m\e[20GShow entry's last modification date and time."
	echo -e "  \e[1m--show-username\e[m\e[20GShow entry's username."
	echo -e "  \e[1m--show-password\e[m\e[20GShow entry's password. \e[1;31mWarning: Password will be displayed on screen in plain text\e[m"
	echo -e "  \e[1m--show-url\e[m\e[20GShow entry's URL."
	echo -e "  \e[1m--show-notes\e[m\e[20GShow entry's notes."
	echo -e "  \e[1m--columns[=]<COLUMNS>\e[m"
	echo -e "\e[20GThe name of the columns to be displayed, comma separated. Valid column names are: \e[4mid\e[m, \e[4mname\e[m, \e[4mcdate\e[m, \e[4mmdate\e[m, \e[4musername\e[m, \e[4mpassword\e[m, \e[4murl\e[m and \e[4mnotes\e[m."
	echo ""

################
# Print help for edit entry subcommand
elif [ "$action" == "edit entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4medit\e[m [ARGUMENTS]"
	echo ""
	echo -e "Edit data of an entry in database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m-y|--yes\e[m\e[20GDon't ask for confirmation."
	echo -e "  \e[1m--item[=]<name>\e[m\e[20GThe \e[4m<name>\e[m of the entry to edit."
	echo -e "  \e[1m--itmnn[=]<name>|--item-name[=]<name>\e[m"
	echo -e "\e[20GThe new \e[4m<name>\e[m of the entry."
	echo -e "  \e[1m--itmun[=]<username>|--item-username[=]<username>\e[m"
	echo -e "\e[20GThe new \e[4m<username>\e[m of the entry."
	echo -e "  \e[1m--itmpw[=]<password>|--item-password[=]<password>\e[m"
	echo -e "\e[20GThe new \e[4m<password>\e[m of the entry."
	echo -e "\e[20G\e[1mWARNING:\e[m This will expose the password to bash history if used in command line. Consider removing the history items or use the clear action with --history argument. Otherwise, just don't use this argument and the program will prompt for the password."
	echo -e "  \e[1m--itmurl[=]<url>|--item-url[=]<url>\e[m"
	echo -e "\e[20GThe new \e[4m<url>\e[m of the entry."
	echo -e "  \e[1m--itmnt[=]<notes>|--item-notes[=]<notes>\e[m"
	echo -e "\e[20GThe new \e[4m<notes>\e[m string of the entry."

	echo -e "  \e[1m--delete-username|--dun\e[m"
	echo -e "\e[20GDelete the \e[4musername\e[m field of the selected entry"
	echo -e "  \e[1m--delete-password|--dpw\e[m"
	echo -e "\e[20GDelete the \e[4mpassword\e[m field of the selected entry."
	echo -e "  \e[1m--delete-url|--dul\e[m"
	echo -e "\e[20GDelete the \e[4murl\e[m field of the selected entry."
	echo -e "  \e[1m--delete-notes|--dnt\e[m"
	echo -e "\e[20GDelete the \e[4mnotes\e[m field of the selected entry."
	echo ""

################
# Print help for del entry subcommand
elif [ "$action" == "del entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mdel\e[m [ARGUMENTS]"
	echo ""
	echo -e "Delete an entry from the database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m-y|--yes\e[m\e[20GDon't ask for confirmation."
	echo -e "  \e[1m--item[=]<name>\e[m\e[20GThe \e[4m<name>\e[m of the entry to delete."
	echo ""

################
# Print help for list entry subcommand
elif [ "$action" == "list entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mlist\e[m [ARGUMENTS]"
	echo ""
	echo -e "Shows all or a range of the entries in the database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m-b=<NUMBER>|--begin=<NUMBER>\e[m"
	echo -e "\e[20GBegin from <NUMBER> of item."
	echo -e "  \e[1m-l=<NUMBER>|--length=<NUMBER>\e[m"
	echo -e "\e[20GLimit results to <NUMBER> of items."
	echo ""

################
# Print help for clear subcommand
elif [ "$action" == "clear help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m \e[4mclear\e[m [ARGUMENTS]"
	echo ""
	echo -e "Clears data from clipboard and terminal history."
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m--clipboard=<TMPFILE>\e[m"
	echo -e "\e[20GReads the <TMPFILE> and if it's contents match with the contents of the clipboard then it clears the clipboard. This will delete the <TMPFILE> in any case."
	echo -e "  \e[1m--history\e[m\e[20GClears terminal history from entries containing passman."
	echo ""

################
# Print help for export entries subcommand
elif [ "$action" == "export entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mexport\e[m [ARGUMENTS] [EXPORT_FILE]"
	echo ""
	echo -e "Export data from database in the selected format."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m-b=<NUMBER>|--begin=<NUMBER>\e[m"
	echo -e "\e[20GBegin export items from <NUMBER> of item."
	echo -e "  \e[1m-l=<NUMBER>|--length=<NUMBER>\e[m"
	echo -e "\e[20GLimit exported results to <NUMBER> of items."
	echo -e "  \e[1m-f=<TYPE>|--format=<TYPE>\e[m"
	echo -e "\e[20GSet the format <TYPE> to be used for the exported data."
	echo -e "\e[20GValid types can be: \e[4mxml\e[m, \e[4mjson\e[m, \e[4msql\e[m, \e[4mcsv\e[m and \e[4mtxt\e[m"
	echo -e " \e[1m--overwrite\e[m"
	echo -e "\e[20GIf output file exists, overwrite it without asking user."
	echo ""
	echo -e "\e[1;4mEXPORT_FILE:\e[m"
	echo -e " (Optional) If this argument is set, all data that are supposed to be exported will be saved to this file. If the file doesn't exist the file will be created. If the file already exists then it will be overwritten. If the path to the file does not exist or is inaccessible the script will stop and will return error code \e[1m121\e[m."
	echo ""

################
# Print help for import entries subcommand
elif [ "$action" == "import entry help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mimport\e[m [ARGUMENTS]"
	echo ""
	echo -e "Import data from a file or string to the database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m-f[=]<file>|--file[=]<file>\e[m"
	echo -e "\e[20GThe path of the \e[4m<file>\e[m to read for importing entries."
	echo -e "  \e[1m-t[=]<type>|--type[=]<type>\e[m"
	echo -e "\e[20GDefine the format \e[4m<type>\e[m of the import data. If this argument is not defined, the program will try to autodetect the data format."
	echo -e "\e[20GValid types can be: \e[4mxml\e[m, \e[4mjson\e[m, \e[4msql\e[m and \e[4mcsv\e[m"
	echo -e "  \e[1m--|--stdin\e[m\e[20GRead string in stdin for importing data."
	echo ""
	
################
# Print help for encrypt subcommand
elif [ "$action" == "encrypt help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mencrypt\e[m [ARGUMENTS]"
	echo ""
	echo -e "Encrypt database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo ""

################
# Print help for decrypt subcommand
elif [ "$action" == "decrypt help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mdecrypt\e[m [ARGUMENTS]"
	echo ""
	echo -e "Decrypt and ecrypted database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo ""

################
# Print help for build subcommand
elif [ "$action" == "build help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mbuild\e[m [ARGUMENTS]"
	echo ""
	echo -e "Create a database file to be used for storing passwords and adds basic settings."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo -e "  \e[1m-h|--help\e[m\e[20GThis screen. All other arguments will be ignored."
	echo ""

################
# Print help for backup subcommnad
elif [ "$action" == "backup help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4mbackup\e[m [ARGUMENTS]"
	echo ""
	echo -e "Show list of backups for an entry, restore a backup or view backup's data."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo ""
	echo -e "  \e[1m-h|--help\e[m\e[25GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m--entry[=]<entry>\e[m\e[25GSelect the \e[4m<entry>\e[m to work with it's backup."
	echo -e "  \e[1m--list\e[m\e[25GPrint a list of the backups for the selected entry."
	echo -e "  \e[1m--restore[=]<number>\e[m\e[25GRestore the the data of selected item \e[4m<number>\e[m to the selected \e[4mentry\e[m."
	echo -e "  \e[1m--view[=]<number>\e[m\e[25GView the data of the selected item \e[4m<number>\e[m of the selected \e[4mentry\e[m."
	echo -e "  \e[1m-rp|--reveal-password\e[m\e[25GReveal passwords on printing data to the screen."
	echo -e "  \e[1m-y|--yes\e[m\e[25GDon't ask for confirmation when restoring a backup."
	echo ""

################
# Print help for setting subcommand
elif [ "$action" == "setting help" ]; then

	echo -e "\e[1;4mUsage:\e[m \e[1m$(basename $0)\e[m [GLOBAL_ARGUMENTS] \e[4msetting\e[m [ARGUMENTS]"
	echo ""
	echo -e "Get and change the values of settings keys in database."
	echo ""
	print_global_arguments_help
	echo ""
	echo -e "\e[1;4mARGUMENTS:\e[m"
	echo ""
	echo -e "  \e[1m-h|--help\e[m\e[25GThis screen. All other arguments will be ignored."
	echo -e "  \e[1m--name[=]<setting>\e[m\e[25GGive the name of the setting to change it's value."
	echo -e "  \e[1m--delete[=]<setting>\e[m\e[25GDelete the value of a \e[4msetting\e[m."
	echo -e "  \e[1m--set[=]<value>\e[m\e[25GSet the given \e[4mvalue\e[m for the setting selected using the \e[4m--name\e[m argument."
	echo -e "  \e[1m--get[=]<setting>\e[m\e[25GGet the value of the \e[4msetting\e[m."
	echo -e "  \e[1m--list\e[m\e[25GGet a list of the setting names in database."
	echo ""

################
# Call passgen script adding "-h" argument
elif [ "$action" == "gen entry help" ]; then

	commandstr="${passgencmd} -h"

	$commandstr

	exit $?

################
# Print Version
elif [ "$action" == "version" ]; then

	echo -e "\e[1m$(basename $0)\e[m Version ${scriptVersion}"
	exit 0

fi
