# Password Manager #

[![License](https://img.shields.io/:license-gpl3-blue.svg)](./LICENSE)

<!-- Version %%VERSION%% -->

PassMan is a password generator and manager for the linux terminal.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
**Table of Contents**

- [Password Manager](#password-manager)
    - [Installation](#installation)
    - [Make Package](#make-package)
        - [Make DEB Package](#make-deb-package)
    - [Variables for setings.sh](#variables-for-setingssh)
    - [Command line](#command-line)
        - [GLOBAL_ARGUMENTS](#global_arguments)
        - [COMMANDS](#commands)
            - [new](#new)
            - [get](#get)
            - [edit](#edit)
            - [del](#del)
            - [list](#list)
            - [gen](#gen)
            - [clear](#clear)
            - [decrypt](#decrypt)
            - [encrypt](#encrypt)
            - [export](#export)
            - [import](#import)
            - [build](#build)
            - [backup](#backup)
            - [setting](#setting)
    - [Exit Status](#exit-status)
    - [Dependencies](#dependencies)

<!-- markdown-toc end -->

## Installation ##

You can install this script using the make command:
```bash
git clone https://gitlab.com/elvizakos/passman
cd passman
make install
```

## Make Package ##

### Make DEB Package ###

```bash
make deb # create deb package
make clean # clean temporary files
```

## Variables for setings.sh ##

The "settings.sh" file is a bash script loaded with **passman** and **passgen** scripts for setting the scripts settings.
The file can exist in "~/.local/etc/passman/settings.sh" and/or "/etc/passman/settings.sh" locations.

| Variable                | Default Value                   | Description                                                                                                                                                                                                               |
|-------------------------|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| dbfilepath              | "~/.config/passman/passman.db"  | This variable sets the path of the database file.                                                                                                                                                                         |
| numberOfEncryptionTimes | 2                               | This variable sets the number of times to encrypt and decrypt the database file.                                                                                                                                          |
| passgencmd              | The same path as passman script | This variable sets the location of **passgen** script.                                                                                                                                                                    |
| encryptCMD              |                                 | This variable sets the command to be used for the encryption of database.<br>In the variable can be used:<br>**%%DBPATH%%** - Replaced with the path of the database.<br>**%%DBPASS%%** - Replaced with the password.     |
| decryptCMD              |                                 | This  variable  sets  the  command to be used for the decryption of database.<br>In the variable can be used:<br>**%%DBPATH%%** - Replaced with the path of the database.<br>**%%DBPASS%%** - Replaced with the password. |

## Command line ##

The basic syntax is:
```bash
passman [GLOBAL_ARGUMENTS] COMMAND [ARGUMENTS]
```

### GLOBAL_ARGUMENTS ###

| Argument                                         | Description                                                                                                                                                                                                                                                                                                                                                                                       |
|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>-h</kbd>, <kbd>--help</kbd>                 | Prints help screen.                                                                                                                                                                                                                                                                                                                                                                               |
| <kbd>-v</kbd>, <kbd>--version</kbd>              | Prints the version number to the screen.                                                                                                                                                                                                                                                                                                                                                          |
| <kbd>--password=PASSWORD</kbd>                   | Sets  the  password to be used to open the database. If password is needed but this argument isn't used it will prompt for it. **WARNING: This will expose the password to bash history if used in command line. Consider removing the history items or use the *clear* action with *--history* argument.** Otherwise, just don't use this argument and the program will prompt for the password. |
| <kbd>--db=PATH</kbd>, <kbd>--database=PATH</kbd> | Sets the path of the database to be used.                                                                                                                                                                                                                                                                                                                                                         |

### COMMANDS ###

#### new ####

Creates a new entry.

| Argument                                                          | Optional | Description                                                                                                                                                                                                                                                                                        |
|-------------------------------------------------------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>                                  | Yes      | Show help for the new entry subcommand. If  this  is  set, all other arguments will be ignored.                                                                                                                                                                                                    |
| <kbd>--yes</kbd>, <kbd>-y</kbd>                                   | Yes      | Don't ask for confirmation for creating entry.                                                                                                                                                                                                                                                     |
| <kbd>--item=ITEM_NAME</kbd>                                       | No       | Sets the name of the entry.                                                                                                                                                                                                                                                                        |
| <kbd>--itmun=USERNAME</kbd>, <kbd>--item-username=USERNAME</kbd>  | Yes      | Sets the username of the entry.                                                                                                                                                                                                                                                                    |
| <kbd>--itmpw=PASSWORD</kbd>, <kbd>--item-password=PASSSWORD</kbd> | Yes      | Sets the password of the entry. **WARNING: This will expose the password to bash history if used in command line. Consider removing the history items or use the *clear* action with *--history* argument.** Otherwise, just don't use this argument and the program will prompt for the password. |
| <kbd>--itmurl=URL</kbd>, <kbd>--item-url=URL</kbd>                | Yes      | Sets the url of the entry.                                                                                                                                                                                                                                                                         |
| <kbd>--itmnt=NOTES</kbd>, <kbd>--item-notes=NOTES</kbd>           | Yes      | Sets some notes about the entry.                                                                                                                                                                                                                                                                   |

#### get ####

Get data from an entry in the database.

**Warning: All data, including passwords, will be displayed in plain text to the screen.**

| Argument                         | Optional | Description                                                                                                                                                                                            |
|----------------------------------|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the get entry subcommand. If  this  is  set, all other arguments will be ignored.                                                                                                        |
| <kbd>--item=NAME</kbd>           | No       | Give the name of the entry to be displayed.                                                                                                                                                            |
| <kbd>--clip=FIELD</kbd>          | Yes      | Copy a field from an entry to clipboard. Don't show anything to the screen.<br> Valid field names are: <kbd>name</kbd>, <kbd>username</kbd>, <kbd>password</kbd>, <kbd>url</kbd> and <kbd>notes</kbd>. |
| <kbd>--show-id</kbd>             | Yes      | Display the database ID of the entry.                                                                                                                                                                  |
| <kbd>--show-name</kbd>           | Yes      | Display the name of the entry.                                                                                                                                                                         |
| <kbd>--show-cdate</kbd>          | Yes      | Display the entry's creation date and time.                                                                                                                                                            |
| <kbd>--show-mdate</kbd>          | Yes      | Display the entry's last modification date and time.                                                                                                                                                   |
| <kbd>--show-username</kbd>       | Yes      | Display the entry's username.                                                                                                                                                                          |
| <kbd>--show-password</kbd>       | Yes      | Display the entry's password. **Warning: the password will be displayed to the screen in plain text.**                                                                                                 |
| <kbd>--show-url</kbd>            | Yes      | Display the entry's URL.                                                                                                                                                                               |
| <kbd>--show-notes</kbd>          | Yes      | Display the entry's notes.                                                                                                                                                                             |
| <kbd>--columns=COLUMNS</kbd>     | Yes      | A string of comma separated, column names to be displayed to the screen. alid column names are:  *id*,  *name*,  *cdate*,  *mdate*,  *username*, *password*, *url* and *notes*.                        |

#### edit ####

Modify data of an entry in the database.

| Argument                                                         | Optional | Description                                                                                                                                                                                                                                                                                                    |
|------------------------------------------------------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>                                 | Yes      | Show help for the edit entry subcommand. If  this  is  set, all other arguments will be ignored.                                                                                                                                                                                                               |
| <kbd>--item=NAME</kbd>                                           | No       | The name of an existing entry that will be modified.                                                                                                                                                                                                                                                           |
| <kbd>--yes</kbd>, <kbd>-y</kbd>                                  | Yes      | Don't ask for confirmation for modifying the entry.                                                                                                                                                                                                                                                            |
| <kbd>--itmnm=NAME</kbd>, <kbd>--item-name=NAME</kbd>             | Yes      | Replace the entry's name with new name.                                                                                                                                                                                                                                                                        |
| <kbd>--itmun=USERNAME</kbd>, <kbd>--item-username=USERNAME</kbd> | Yes      | Replace entry's username with new username.                                                                                                                                                                                                                                                                    |
| <kbd>--itmpw=PASSWORD</kbd>, <kbd>--item-password=PASSWORD</kbd> | Yes      | Replace entry's password with new password. **WARNING: This will expose the password to bash history if used in command line. Consider removing the history items or use the *clear* action with *--history* argument.** Otherwise, just don't use this argument and the program will prompt for the password. |
| <kbd>--itmurl=URL</kbd>, <kbd>--item-url=URL</kbd>               | Yes      | Replace entry's url with new url.                                                                                                                                                                                                                                                                              |
| <kbd>--itmnt=NOTES</kbd>, <kbd>--item-notes=NOTES</kbd>          | Yes      | Replace entry's notes string with a new one.                                                                                                                                                                                                                                                                   |
| <kbd>--delete-username</kbd>, <kbd>--dun</kbd>                   | Yes      | Delete the username field of the selected entry.                                                                                                                                                                                                                                                               |
| <kbd>--delete-password</kbd>, <kbd>--dpw</kbd>                   | Yes      | Delete the password field of the selected entry.                                                                                                                                                                                                                                                               |
| <kbd>--delete-url</kbd>, <kbd>--dul</kbd>                        | Yes      | Delete the url field of the selected entry.                                                                                                                                                                                                                                                                    |
| <kbd>--delete-notes</kbd>, <kbd>--dnt</kbd>                      | Yes      | Delete the notes field of the selected entry.                                                                                                                                                                                                                                                                  |

#### del ####

Delete and entry from the database.

| Argument                         | Optional | Description                                                                             |
|----------------------------------|----------|-----------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the "del" subcommand.If this is set, all other arguments will be ignored. |
| <kbd>--yes</kbd>, <kbd>-y</kbd>  | Yes      | Don't ask for confirmation for deleting the entry.                                      |
| <kbd>--item=NAME</kbd>           | No       | Set the NAME of the entry to delete.                                                    |

#### list ####

Shows a list of all or a range of the entries in the database.

| Argument                                         | Optional | Description                                                                               |
|--------------------------------------------------|----------|-------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>                 | Yes      | Show help for the "list" subcommand. If this is set, all other arguments will be ignored. |
| <kbd>-b=NUMBER</kbd>, <kbd>--begin=NUMBER</kbd>  | Yes      | Set the number of the item to begin listing.                                              |
| <kbd>-l=NUMBER</kbd>, <kbd>--length=NUMBER</kbd> | Yes      | Set the number of results to show.                                                        |

#### gen ####

Runs **passgen** script. All following arguments are  passed  to  this script.

The exit status of passgen script will be returned.

#### clear ####

Clears data from clipboard and terminal history.

| Argument                          | Optional | Description                                                                                                                                                          |
|-----------------------------------|----------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>  | Yes      | Show help for this subcommand. If this is set,  all other arguments will be ignored.                                                                                 |
| <kbd>--clipboard=temp\_file</kbd> | Yes      | Reads  the  temp\_file and if it's contents matches with the contents of the clipboard then it  clears  the  clipboard. This will delete the temp\_file in any case. |
| <kbd>--history</kbd>              | Yes      | Clears  the  terminal history from entries containing commands of *passman*.                                                                                                                                                                     |

#### decrypt ####

Decrypts the selected database.

| Argument                         | Optional | Description                                                                                  |
|----------------------------------|----------|----------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the "decrypt" subcommand. If this is set, all other arguments will be ignored. |

#### encrypt ####

Encrypts the selected database.

| Argument                         | Optional | Description                                                                                  |
|----------------------------------|----------|----------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the "encrypt" subcommand. If this is set, all other arguments will be ignored. |

#### export ####

Export data from database in the selected format.

**WARNING**: All data, including the passwords of the entries, will be shown in plain text.

If the last argument is a path to a file the program will export
the data into that file. If the file doesn't exist, then it will
create that file. If it does exist then it will replace it. If the path does not exist then it will stop and return error code **121**.
		   
| Argument                                         | Optional | Description                                                                                                             |
|--------------------------------------------------|----------|-------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>                 | Yes      | Show help for the "export" subcommand. If this is set, all other arguments will be ignored.                             |
| <kbd>-b=NUMBER</kbd>, <kbd>--begin=NUMBER</kbd>  | Yes      | Set the number of the item to begin exporting.                                                                          |
| <kbd>-l=NUMBER</kbd>, <kbd>--length=NUMBER</kbd> | Yes      | Set the number of results to export.                                                                                    |
| <kbd>-f=TYPE</kbd>, <kbd>--format=TYPE</kbd>     | No       | Set the format type for the data to be exported. Valid formats can be: **xml**, **json**, **sql**, **csv** and **txt**. |
| <kbd>--overwrite</kbd>                           | Yes      | If output file exists, overwrite it without asking user.                                                                |

#### import ####
Import data from a file or stdin string to database.

| Argument                                   | Optional | Description                                                                                                                                                                                      |
|--------------------------------------------|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>           | Yes      | Show help for the "import" subcommand. If this is set, all other arguments will be ignored.                                                                                                      |
| <kbd>-f=FILE</kbd>, <kbd>--file=FILE</kbd> | Yes      | The path of the file to read for importing entries from.                                                                                                                                         |
| <kbd>-t=TYPE</kbd>, <kbd>--type=TYPE</kbd> | Yes      | The format type of the file or string to read. If this argument is not set, the program will try to autodetect the correct format type. Valid types are: **xml**, **json**, **sql** and **csv**. |
| <kbd>--</kbd>, <kbd>--stdin</kbd>          | Yes      | Read data from the **stdin** string.                                                                                                                                                             |

#### build ####
Creates a database file to be used for storing passwords and adds basic settings to it.

| Argument                         | Optional | Description                                                                                |
|----------------------------------|----------|--------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the "build" subcommand. If this is set, all other arguments will be ignored. |

#### backup ####
Show list of backups for an entry, restore a backup or view backup's data.

| Argument                                     | Optional | Description                                                                                 |
|----------------------------------------------|----------|---------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd>             | Yes      | Show help for the "backup" subcommand. If this is set, all other arguments will be ignored. |
| <kbd>--entry=entry</entry>                   | No       | Select the entry to work with it's backup.                                                  |
| <kbd>--list</kbd>                            | Yes      | Print a list of the backups for the selected entry.                                         |
| <kbd>--view=itemNumber</kbd>                 | Yes      | View the data of the selected itemNumber of the selected entry.                             |
| <kbd>--restore=itemNumber</kbd>              | Yes      | Restore the the data of selected itemNumber to the selected entry.                          |
| <kbd>--reveal-password</kbd>, <kbd>-rp</kbd> | Yes      | Reveal passwords on printing data to the screen.                                            |
| <kbd>--yes</kbd>, <kbd>-y</kbd>              | Yes      | Don't ask for confirmation when restoring a backup.                                         |

#### setting ####
Get and change the values of settings keys in database.

| Argument                         | Optional | Description                                                                                  |
|----------------------------------|----------|----------------------------------------------------------------------------------------------|
| <kbd>--help</kbd>, <kbd>-h</kbd> | Yes      | Show help for the "setting" subcommand. If this is set, all other arguments will be ignored. |
| <kbd>--name=settingName</kbd>    | Yes      | Set the name of the setting to be changed using the <kbd>--set</kbd> argument.               |
| <kbd>--delete=settingName</kbd>  | Yes      | Delete the setting named "settingName".                                                      |
| <kbd>--set=value</kbd>           | Yes      | Set the value for the setting selecte in <kbd>--name</kbd> argument.                         |
| <kbd>--get=settingName</kbd>     | Yes      | Get the value of the settingName.                                                            |
| <kbd>--list</kbd>                | Yes      | Get a list of the setting names in the database.                                             |

## Exit Status ##

| Code | Description                                                                                   |
|------|-----------------------------------------------------------------------------------------------|
| 120  | Imported file type is not supported.                                                          |
| 121  | Database file does not exist or is inaccessible.                                              |
| 122  | For new command, entry already exists.<br>For edit and delete commands, entry does not exist. |
| 123  | Dependency does not exist.                                                                    |
| 124  | Wrong password for database.                                                                  |
| 125  | Unknown command or argument.                                                                  |

## Dependencies ##

| Command    | Mandatory | Description                                                                                     |
|------------|-----------|-------------------------------------------------------------------------------------------------|
| bash       | Yes       | Necessary for running the script                                                                |
| sqlite3    | Yes       | Necessary for reading writing the database. Needed for importing data from sql files or strings |
| base64     | Yes       | Used for storing entries to the database                                                        |
| gpg        | Yes       | Necessary for encrypting the database                                                           |
| openssl    | Yes       | Necessary for encrypting the database                                                           |
| xclip      | No        | Necessary for controling the clipboard                                                          |
| xsel       | No        | Necessary for controling the clipboard                                                          |
| pbcopy     | No        | Necessary for controling the clipboard                                                          |
| cygwin     | No        | Necessary for controling the clipboard on windows environment                                   |
| passgen    | No        | Used for generating passwords                                                                   |
| rpmbuild   | No        | Needed for creating rpm packages                                                                |
| dpkg-deb   | No        | Needed for creating deb packages                                                                |
| xmlstarlet | No        | Needed for importing data from xml files or strings                                             |
| jq         | No        | Needed for importing data from json files or strings                                            |
| at         | No        | Needed for tasks for cleaning data.                                                             |
