2022-09-03  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add "at" dependency.

2022-08-26  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to v1.3.14-beta

	* src/passman.sh: Keep database unencrypted for a defined number of minutes for convenience, unless the defined number is 0 (zero).

2022-08-23  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add setting "keep_db_unencrypted_for_minutes".

2022-08-09  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to v1.2.13-beta

	* src/passman.sh: Add argument "--history" for the "clear" subcommand.

2022-08-08  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.2.12-beta

	* README.md: Add help for the "--overwrite" argument of the "export" subcommnad.

	* src/passman.man: Add help for the "--overwrite" argument of the "export" subcommand.

	* src/passman.sh: Add "--overwrite" argument for overwritting the output file for the "export" subcommand.
	Add question for overwritting the output file for the "export" subcommand if file exists and if the "--overwrite" argument is not set.
	Add help for the "--overwrite" argument of the "export" subcommand.

2022-08-04  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add warnings on help about password being exposed.

	* src/passman.man: Add warnings about password being exposed.

	* src/passman.sh: On help and version show only the executalbe's filename, not full path.

2022-08-02  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add warnings for typing passwords in command line.

2022-08-01  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.2.11-beta

	* Makefile: Add target "install-user" for installing app for current user only.
	(remove-user): Add target "remove-user" for uninstalling app if it was installed for current user only but keep the apps data.
	(purge-user): Add target "purge-user" for uninstalling app if it was installed for current user only and delete all apps data.

2022-07-29  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.10-beta

	* README.md: Add help for the "--delete-username" and "--dun" arguments of the "edit" subcommand.
	Add help for the "--delete-password" and "--dpw" arguments of the "edit" subcommand.
	Add help for the "--delete-url" and "--dul" arguments of the "edit" subcommand.
	Add help for the "--delete-notes" and "--dnt" arguments of the "edit" subcommand.

	* src/passman.sh: Add help for the "--delete-username" and "--dun" arguments of the "edit" subcommand.
	Add help for the "--delete-password" and "--dpw" arguments of the "edit" subcommand.
	Add help for the "--delete-url" and "--dul" arguments of the "edit" subcommand.
	Add help for the "--delete-notes" and "--dnt" arguments of the "edit" subcommand.

	* src/passman.man: Add help for the "--delete-username" and "--dun" arguments of the "edit" subcommand.
	Add help for the "--delete-password" and "--dpw" arguments of the "edit" subcommand.
	Add help for the "--delete-url" and "--dul" arguments of the "edit" subcommand.
	Add help for the "--delete-notes" and "--dnt" arguments of the "edit" subcommand.

	* src/passman.sh: Add argument "--delete-username" and "--dun" for the "edit" subcommand for deleting the contents of "username" field of the selected entry.
	Add argument "--delete-password" and "--dpw" for the "edit" subcommand for deleting the contents of "password" field of the selected entry.
	Add argument "--delete-url" and "--dul" for the "edit" subcommand for deleting the contents of "url" field of the selected entry.
	Add argument "--delete-notes" and "--dnt" for the "edit" subcommand for deleting the contents of "notes" field of the selected entry.

	* VERSION set to 1.1.9-alpha

	* src/passman.sh: Fix modifiy entry dialog.

2022-07-28  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Fix some relative paths.

2022-04-12  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add "setting" subcommand to the main help screen.

2022-04-08  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Refresh TOC.

	* VERSION set to 1.1.8-alpha

	* README.md: Add help for the "setting" subcommand.
	Add help for the "-h" and "--help" arguments of the "setting" subcommand.
	Add help for the "--name" argument of the "setting" subcommand.
	Add help for the "--delete" argument of the "setting" subcommand.
	Add help for the "--set" argument of the "setting" subcommand.
	Add help for the "--get" argument of the "setting" subcommand.
	Add help fot the "--list" argument of the "setting" subcommand.

	* src/passman.man: Add help for the "setting" subcommand.
	Add help for the "-h" and "--help" arguments of the "setting" subcommand.
	Add help for the "--name" argument of the "setting" subcommand.
	Add help for the "--delete" argument of the "setting" subcommnad.
	Add help for the "--set" argument of the "setting" subcommand.
	Add help for the "--get" argument of the "setting" subcommand.
	Add help for the "--list" argument of the "setting" subcommand.

	* src/passman.sh: Add argument "--name" for the "setting" subcommand.
	Add argument "--delete" for the "setting" subcommand.
	Add argument "--set" for the "setting" subcommand.
	Add argument "--get" for the "setting" subcommand.
	Add argument "--list" for the "setting" subcommand.
	Add arguments "-h" and "--help" for the "setting" subcommand.
	Add help for the subcommand "setting".
	Add help for the arguments "-h" and "--help" of the "setting" subcommand.
	Add help for the argument "--name" of the "setting" subcommand.
	Add help for the argument "--delete" of the "setting subcommand.
	Add help for the argument "--set" of the "setting" subcommand.
	Add help for the argument "--get" of the "setting" subcommand.
	Add help for the argument "--list" of the "setting" subcommand.

2022-04-07  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.7-alpha

	* src/passman.sh: Add subcommand "setting" for working with settings in database.

2022-04-04  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.6

	* Makefile (VERSION): set to 1.1.6

	* README.md: Add help for the "-y" and "--yes" arguments of the "backup" subcommand.

	* src/passman.man: Add help for the "-y" and "--yes" arguments of the "backup" subcommand.

	* src/passman.sh: Add help for the "-y" and "--yes" arguments of the "backup" subcommand.

2022-04-03  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add arguments "--yes" and "-y" for the "backup" subcommand.

2022-04-02  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.5

	* README.md: Add help for the "backup" subcommand.
	Add help for the "-h" and "--help" arguments of the "backup" subcommand.
	Add help for the "--entry" argument of the "backup" subcommand.
	Add help for the "--list" argument of the "backup" subcommand.
	Add help for the "--view" argument of the "backup" subcommand.
	Add help for the "--restore" argument of the "backup" subcommand.
	Add help for the "--reveal-password" and "-rp" arguments of the "backup" subcommand.
	Refresh TOC.

	* src/passman.man: Add help for the "backup" subcommand.
	Add help for the "-h" and "--help" arguments of the "backup" subcommand.
	Add help for the "--entry" argument of the "backup" subcommand.
	Add help for the "--list" argument of the "backup" subcommand.
	Add help for the "--restore" argument of the "backup" subcommand.
	Add help for the "--restore" argument of the "backup" subcommand.
	Add help for the "-rp" and "--reveal-password" arguments of the "backup" subcommand.

	* src/passman.sh: Fix restoring backup.

2022-04-01  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add help for the "--entry" argument of the "backup" subcommand.
	Add help for the "--list" argument of the "backup" subcommand.
	Add help for the "--restore" argument of the "backup" subcommand.
	Add help for the "--view" argument of the "backup subcommand.
	Add help for the "-rp" and "--reveal-password" arguments of the "backup" subcommand.
	Remove the "--item" argument of the "backup" subcommand.
	Add "--entry" argument for the "backup" subcommand.
	(restoreBackup): Add function "restoreBackup".
	(restoreBackup): Add documentation for function "restoreBackup".

2022-03-30  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.4-alpha

	* src/passman.sh: Add "backup" subcommand.
	Add "--list" argument for the "backup" subcommand.
	Add "--restore" argument for the "backup" subcommand.
	Add "--item" argument for the "backup" subcommand.
	Add "--help" and "-h" arguments for the "backup" subcommand.
	Add "--view" argument for the "backup" subcommand.
	Add function "viewBackupEntryData" for displaying to screen the data of a backup item
	Add "--reveal-password" and "-rp" arguments for the "backup" subcommand.

2022-03-29  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.1.3

	* Makefile (VERSION): set version to 1.1.3

	* README.md: Add help for the "build" subcommand.

	* src/passman.man: Add help for the "build" subcommand.

	* src/passman.sh: Add help for the "build" subcommand.

	* VERSION set to 1.1.2

	* Makefile (VERSION): set version to 1.1.2

	* src/passman.sh (delSetting): Add function "delSetting" for deleting a setting entry in databse.
	(delSetting): Add documentation for "delSetting function.
	(getListOfBackupsForEntry): Add function "getListOfBakupsForEntry" for getting the list of backup items per entry.
	(getListOfBackupsForEntry): Add documentation for "getListOfBackupsForEntry" function.
	(backupItem): Add function "backupItem" for backing up and entry.
	(backupItem): Add documentation for "backupItem" function.

	* VERSION set to 1.1.1

	* src/passman.sh: Add "build" subcommand.

2022-03-28  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (setSetting): Add function "setSetting" for adding and modifying settings in database.
	(setSetting): Add documentation for "setSetting" function.
	(getSetting): Add function "getSetting" for getting the value of the setting entry in the database.
	(setSetting): Add documentation for "getSetting" function.

2022-03-27  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (editItem): Keep backup of the entry before modifying it.
	(deleteItem): Keep backup of the entry before deleting it.

2022-03-15  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (createDatabase): Add "backups" table for keeping entries backups.
	(backupItem): Add "backupItem" function for keeping backups of the entries.

2022-03-14  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add help on exporting data to file for the "export" subcommand.

2022-03-11  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.man: Add help on exporting data to file for the "export" subcommand.

2022-03-10  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: If the last argument of the "export" subcommand is a filepath, export data to that file.
	Add help for exporting to file.

2022-03-02  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 1.0.0

	* README.md: Add help for the "clear" subcommand.
	Add help for "-h" and "--help" arguments of the "clear" subcommand.
	Add help for "--clipboard" argument of the "clear" subcommand.
	Add help for "--history" argument of the "clear" subcommand.

	* src/passman.man: Add help for the "clear" subcommand.
	Add help for "-h" and "--help" arguments of the "clear" subcommand.
	Add help for "--clipboard" argument of the "clear" subcommand.
	Add help for "--history" argument of the "clear" subcommand.

	* src/passman.sh: Add help for "clear" subcommand.
	Add "-h" and "--help" arguments for the clear subcommand.

2022-03-01  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 0.13.33

	* src/passman.sh (clipcopy): Change "dependency does not exists" exit code from 4 to 123cx
	(clippaste): Change "dependency does not exists" exit code from 4 to 123
	Fix clearing password from clipboard.

2022-02-28  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 0.13.32

	* src/passman.man: Fix help for the "--clip" argument of "get" subcommand.

	* README.md: Fix help for the "--clip" argument of "get" subcommand.

	* src/passman.sh: Add shurtcut for "--password" argument as "--pw"
	Fix help for the "--clip" argument of "get" subcommand.

2022-02-27  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add variable "clearafter" for setting the number of minutes to clear the clipboard.

2022-02-23  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 0.12.31

	* src/passman.sh: List "import" and "export" subcommands to main help screen.

2022-02-22  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* VERSION set to 0.12.30

	* src/passman.sh: Change the argument reading mechanism.

	* src/passman.man: Add help for exit code 120.

	* README.md: Add help for exit code 120.

	* src/passman.sh (argact): Add help for exit code 120.

	* VERSION set to 0.11.29

	* src/passman.sh: Change exit code 126 to 120

2022-02-20  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add help for the "--show-id" argument of the "get" subcommand.
	Add help for the "--show-name" argument of the "get" subcommand.
	Add help for the "--show-cdate" argument of the "get" subcommand.
	Add help for the "--show-mdate" argument of the "get" subcommand.
	Add help for the "--show-username" argument of the "get" subcommand.
	Add help for the "--show-password" argument of the "get" subcommand.
	Add help for the "--show-url" argument of the "get" subcommand.
	Add help for the "--show-notes" argument of the "get" subcommand.
	Add help for the "--columns" argument of the "get" subcommand.

	* VERSION set to 0.10.27

	* src/passman.man: Add help for "--show-id" argument of the "get" subcommand.
	Add help for "--show-name" argument of the "get" subcommand.
	Add help for "--show-cdate" argument of the "get" subcommand.
	Add help for "--show-mdate" argument of the "get" subcommand.
	Add help for "--show-username" argument of the "get" subcommand.
	Add help for "--show-password" argument of the "get" subcommand.
	Add help for "--show-url" argument of the "get" subcommand.
	Add help for "--show-notes" argument of the "get" subcommand.
	Add help for "--columns" argument of the "get" subcommand.

	* src/passman.sh: Add "--show-id" argument for the "get" subcommand.
	(argact): Add "--show-name" argument for the "get" subcommand.
	(argact): Add "--show-cdate" and "--show-creation-date" arguments for the "get" subcommand.
	(argact): Add "--show-mdate" and "--show-modification-date" arguments for the "get" subcommand.
	(argact): Add "--show-username" argument for the "get" subcommand.
	(argact): Add "--show-password" argument for the "get" subcommand.
	(argact): Add "--show-url" argument for the "get" subcommand.
	(argact): Add "--show-notes" argument for the "get" subcommand.
	(argact): Add "--columns" argument for the "get" subcommand.
	(argact): Add help for the "--show-id" argument of the "get" subcommand.
	(argact): Add help for the "--show-name" argument of the "get" subcommand.
	(argact): Add help for the "--show-cdate" and "--show-creation-date" arguments of the "get" subcommand.
	(argact): Add help for the "--show-mdate" and "--show-modification-date" arguments of the "get" subcommand.
	(argact): Add help for the "--show-username" argument of the "get" subcommand.
	(argact): Add help for the "--show-password" argument of the "get" subcommand.
	(argact): Add help for the "--show-url" argument of the "get" subcommand.
	(argact): Add help for the "--show-notes" argument of the "get" subcommand.
	(argact): Add help for the "--columns" argument of the "get" subcommand.

2022-02-18  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add "xmlstarlet" and "jq" dependencies
	Add "rpmbuild" and "dpkg-deb" dependencies
	Add Table Of Contents

	* src/passman.sh: Remove commented out commands for tests

	* src/passman.man: Add the string "(optional)" to "--" and "--stdin" arguments of the "import" subcommand.

	* src/passman.sh (argact): Add capability for reading from stdin data for importing, for the "import" subcommand.

2022-02-17  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md: Add help for the "import" subcommand .

	* src/passman.man: Add help for the "import" subcommand.
	Add help for the "-h" and "--help" arguments of the "import" subcommand.
	Add help for the "-f" and "--file" arguments of the "import" subcommand.
	Add help for the "-t" and "--type" arguments of the "import" subcommand.
	Add help for the "--" and "--stdin" arguments of the "import" subcommand.

	* src/passman.sh (importCSV): Add function "importCSV" for importing CSV data from a string. To be reused for import from file and string.
	(importCSV): Add Documentation for "importCSV" function.
	(importJSON): Add function "importJSON" for importing JSON data from a string. To be reused for import from file and string.
	(importJSON): Add Documentation for "importJSON" function.
	(importXML): Add function "importXML" for importing XML data from a string. To be reused for import from file and string.
	(importXML): Add Documentation for "importXML" function.
	(importXML): Add function "importSQL" for importing SQL data from a string. To be reused for import from file and string.
	(importXML): Add Documentation for "importSQL" function.
	Add help for the "import" subcommand.

2022-02-14  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add "sql" part for import" subcommand.

2022-02-10  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add "json" part for "import" subcommand.
	Add autodetection for the mime-type of the file to be used for importing data.

2022-02-09  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (XMLFieldToVar): Add function "XMLFieldToVar" for unescaping character of an xml field.
	Added "xml" part for "import" subcommand.

2022-02-08  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (itemExists): Add function "itemExists" to check if there is already an item with the given name in database.
	Added "csv" part for "import" subcommand.

2022-02-05  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md (syntax): Add help for the "decrypt" subcommand.

	* src/passman.man: Add help for "-h" and "--help" arguments for the "decrypt" subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for the "decrypt" subcommand.
	Add help for the "decrypt" subcommand.

2022-02-04  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md (syntax): Add help for the "encrypt" subcommand.

	* src/passman.man: Add help for "-h" and "--help" arguments for the "encrypt" subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for the "encrypt" subcommand.
	Add help for the "encrypt" subcommand.

2022-02-03  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.man: Add help for "-h" and "--help" arguments for the "export" subcommand.

	* README.md (syntax): Add help for the "export" subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for the "export" subcommand.
	Add help for the "export" subcommand.

2022-02-01  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md (syntax): Add description for the "gen" subcommand.
	(syntax): Add description for the "decrypt" subcommand.
	(syntax): Add description for the "encrypt" subcommand.

	* src/passman.sh: Add help for the "gen" subcommand.

	* README.md (syntax): Add help for the "list" subcommand.

	* src/passman.man: Add help for "-h" and "--help" arguments of "list"subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for the "list" subcommand.
	Add help for the "list" subcommand.

	* README.md (syntax): Add help for the "del" subcommand.
	Add description for settings.sh file.

	* src/passman.man: Add help for the "-h" and "--help" arguments of the "del" subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for the "del" subcommand.
	Add help for the "del" subcommand.

2022-01-31  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* README.md (syntax): Convert arguments for the "edit" subcommand to table format.
	(syntax): Convert global arguments to table format.
	(syntax): Convert variables in settings.sh to table format
	(syntax): Convert Exit status code to table format

	* src/passman.man: Add help for "-c" and "--clip" arguments of the "get" subcommand.

	* src/passman.sh : Add help for "-c" and "--clip" aruments of the "get" subcommand.

	* README.md (syntax): Test table format for arguments
	(syntax): Convert arguments for the "get" subcommand to table format.

	* src/passman.sh (entryToJSON): Fix function "entryToJSON": It was exporting only name for all columns.

2022-01-30  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh (getItemsEntry): Add function "getItemsEntry" for returning a specific column of an entry.
	Add option for copying the password of an entry to the clipboard instead of printing it on screen.

	* README.md: Add help for subcommands "new", "get" and "edit".

	* src/passman.sh : Add help for "edit" subcommand usage.

	* src/passman.man: Add help for "-h" and "--help" arguments of "edit" subcommand.

	* src/passman.sh: Add "print_global_arguments_help" function for showing help about global arguments.

2022-01-29  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add help for "get" subcommand.

	* src/passman.man: Add help for "-h" and "--help" arguments in "get" subcommand.

	* src/passman.sh: Add "-h" and "--help" arguments for "get" sub command.

2022-01-28  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.sh: Add help for "--itmurl" and "--item-url" arguments for the "new" command.
	Add help for "--itmnt" and "--item-notes" arguments for the "new" command.

2022-01-23  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* src/passman.man: Minor grammar corrections.

	* README.md: Create "./README.md".

	* Makefile: Create "./Makefile"
	Add target named "deb", for creating DEB package
	Add target named "install", for installing the script
	Add target named "remove", for uninstalling the script
	Add target named "purge", for uninstalling the script and removing all data
	Add target named "clean", for cleaning temporary data

	* src/passman.sh: Move script from "./passman.sh" to "./src/passman.sh".

	* src/passman.man: move man page from "./man/passman" to "./src/passman.man"

2021-12-06  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* man/passman: Create man page for "passman"

2021-10-29  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* passman (clipcopy): Function for putting a string to clipboard
	(clippaste): Function for printing the string from the clipboard
	(dbfilepath): Variable for storing default database file path

2021-10-28  elvizakos  <elvizakos@ubuntu.lvzhome.lan>

	* passman: Create project passwordManager
	(scriptVersion): Variable for storing script's version
	(passLength): Variable for setting generated passwords length
	(selectGroups): Variable for selecting character groups
	(charGroupNames): Array variable for storing descriptions about character groups
	(charGroupSortNames): Array variable for storing short names of character groups
	(charGroups): Array variable for storing character groups
	(printError): Function for generating error strings
	(createDatabase): Function for creating database files
	(encryptDB): Function for encrypting database files
	(decryptDB): Function for decrypting database files
	(addItem): Function for adding password entry
	(getItem): Function for getting password entry
	(getListOfItems): Function for getting list of all password entries

